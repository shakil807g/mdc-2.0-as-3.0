package com.mdconsults.healthprovider.cases;

import com.mdconsults.base.BaseFragment;
import com.mdconsults.di.AppComponent;
import com.mdconsults.widget.ActionBar;

/**
 * Created by Shakil Karim on 5/16/17.
 */

public class DoctorsAppointmentTimeFragment extends BaseFragment {

    @Override
    public int getLayoutID() {
        return 0;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void provideInjection(AppComponent appComponent) {

    }
}

package com.mdconsults.healthprovider.cases;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.mdconsults.R;
import com.mdconsults.base.ImageUploadFragment;
import com.mdconsults.data.model.GroupAndUser;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.data.remote.model.GroupItem;
import com.mdconsults.data.remote.model.UserNames;
import com.mdconsults.data.remote.request_model.CaseCreated;
import com.mdconsults.di.AppComponent;
import com.mdconsults.healthprovider.cases.adapter.ImagePreviewAdapter;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.Utils;
import com.mdconsults.util.VerticalSpaceItemDecoration;
import com.mdconsults.widget.ActionBar;
import com.mdconsults.widget.CustomSpinnerAdapter;
import com.mdconsults.widget.HorizontalFlowLayout;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Shakil Karim on 4/8/17.
 */

public class CreateCaseFragment extends ImageUploadFragment implements Validator.ValidationListener {

    private static final String TAG = "CreateCaseFragment";
    public static int scrollX = 0;
    public static int scrollY = -1;
    @BindView(R.id.lvResults)
    RecyclerView lvResults;
    @BindDimen(R.dimen.item_space_small)
    int listviewSpace;
    @BindView(R.id.main_scrollview)
    NestedScrollView mainScrollview;
    int check = 0;
    @BindView(R.id.txt_patient_id)
    EditText txtPatientId;

    @NotEmpty
    @BindView(R.id.txt_firstname)
    EditText txtFirstname;

    @NotEmpty
    @BindView(R.id.txt_lastname)
    EditText txtLastname;

    @BindView(R.id.txt_age_years)
    EditText txtAgeYears;

    @BindView(R.id.txt_age_month)
    EditText txtAgeMonth;

    @BindView(R.id.txt_current_complain)
    EditText txtCurrentComplain;

    @BindView(R.id.txt_past_history)
    EditText txtPastHistory;

    @BindView(R.id.txt_comments)
    EditText txtComments;
    @BindView(R.id.spin_location)
    Spinner spinLocation;

    @BindView(R.id.referrerItems)
    HorizontalFlowLayout referrerItems;

    @BindView(R.id.referview)
    Spinner referview;

    @Inject
    ApiFactory apiFactory;

    @Inject
    OkHttpClient okHttpClient;

    String locationId;

    int genderPosition;
    List<Integer> groups = new ArrayList<>();
    List<Integer> users = new ArrayList<>();
    private ImagePreviewAdapter adapter;
    private Validator validator;
    private ArrayAdapter arrayAdapter;
    private CustomSpinnerAdapter<GroupAndUser> adapter1;

    public static CreateCaseFragment newInstance() {
        CreateCaseFragment fragment = new CreateCaseFragment();
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);
        setRetainInstance(true);

    }





    @Override
    public int getLayoutID() {
        return R.layout.fragment_create_case;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        scrollX = mainScrollview.getScrollX();
        scrollY = mainScrollview.getScrollY();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetScrollView(scrollX, scrollY);
    }


    public void resetScrollView(int x, int y) {
        ViewTreeObserver vto = mainScrollview.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                mainScrollview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                mainScrollview.scrollTo(x, y);
            }
        });
    }


    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState: ");
        outState.putIntArray("SCROLL_POSITION",
                new int[]{mainScrollview.getScrollX(), mainScrollview.getScrollY()});
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(TAG, "onActivityCreated: ");

        initLocation();
        getGroupsandUsers();


        if (savedInstanceState != null) {
            final int[] position = savedInstanceState.getIntArray("SCROLL_POSITION");
            if (position != null) {
                resetScrollView(position[0], position[1]);
            }


        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initList();

        Log.d(TAG, "onViewCreated: ");


    }


    private  void initLocation(){
        apiFactory.locations()
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(locationResponse -> {

                        List<String> list = new ArrayList<String>();
                        list.add("Select Location");
                        for (int i = 0; i < locationResponse.data.size(); i++) {
                            list.add(locationResponse.data.get(i).getArea());
                        }


                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(),
                                android.R.layout.simple_spinner_item, list);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinLocation.setAdapter(spinnerArrayAdapter);
                        spinLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                if(position == 0) { locationId = null; return; }

                                locationId = locationResponse.data.get(position-1).getId();
                                Log.d(TAG, "initLocation: "+locationId);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                Log.d(TAG, "initLocation: ");
                            }
                        });


                    });



    }

    private void getGroupsandUsers(){

        Observable.zip(
                apiFactory.groupByUsername(getUser().getId()),
                apiFactory.usersNames(getUser().licenses_id == null ? "-1" : getUser().licenses_id),
                (groupItemResponse, userResponse) -> {

                    ArrayList<GroupAndUser> list = new ArrayList<>();

                    for (int i = 0; i < groupItemResponse.data.size(); i++) {
                        GroupItem grop = groupItemResponse.data.get(i);
                        list.add(new GroupAndUser(grop.getGroupId(),"group",grop.getName()));
                    }

                    for (int j = 0; j < userResponse.data.size() ; j ++) {
                        UserNames user = userResponse.data.get(j);
                        list.add(new GroupAndUser(user.getId(),"user",user.getName()));
                    }

                    return list;
                })
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(groupAndUsers -> {
                    adapter1 = new CustomSpinnerAdapter<>(getContext(),groupAndUsers);
                    referview.setAdapter(adapter1);

                },Throwable::printStackTrace);

    }


    private void initList() {
        adapter = new ImagePreviewAdapter(getActivity());
        lvResults.setAdapter(adapter);
        lvResults.setHasFixedSize(true);
        lvResults.setItemAnimator(new DefaultItemAnimator());
        lvResults.addItemDecoration(new VerticalSpaceItemDecoration(listviewSpace));
        lvResults.setNestedScrollingEnabled(false);
        lvResults.setLayoutManager(new GridLayoutManager(getContext(), 2));

    }

    @OnClick(R.id.camera_fab)
    public void onViewClicked() {
        showImageDialog();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        adapter.addImage(list);
    }


    @Override
    public void onValidationSucceeded() {

        if(TextUtils.isEmpty(locationId)){
            Toast.makeText(getContext(), "Location is not selected", Toast.LENGTH_SHORT).show();
            return;
        }

        if(genderPosition == 0 ){
            Toast.makeText(getContext(), "Gender is not selected", Toast.LENGTH_SHORT).show();
            return;
        }


        if(TextUtils.isEmpty(txtAgeYears.getText().toString()) && TextUtils.isEmpty(txtAgeMonth.getText().toString())) {
            Toast.makeText(getContext(), "Please enter age in years or months", Toast.LENGTH_SHORT).show();
            return;
        }


        String year = TextUtils.isEmpty(txtAgeYears.getText().toString()) ? "0" : txtAgeYears.getText().toString();
        if(Integer.valueOf(year) > 100) {
            Toast.makeText(getContext(), "Year cannot be greater than 100", Toast.LENGTH_SHORT).show();
            return;
        }
        String month = TextUtils.isEmpty(txtAgeMonth.getText().toString()) ? "0" : txtAgeMonth.getText().toString();
        if(Integer.valueOf(month) > 11) {
            Toast.makeText(getContext(), "Month cannot be greater than 11", Toast.LENGTH_SHORT).show();
            return;
        }

        CaseCreated caseCreated = new CaseCreated(
                txtPatientId.getText().toString(),
                txtFirstname.getText().toString(),
                txtLastname.getText().toString(),
                genderPosition == 1 ? "m" : "f",year,
                month,
                "1",
                locationId,txtPastHistory.getText().toString(),"",
                "1","1","0",getUser().getId(),txtCurrentComplain.getText().toString(),
                txtComments.getText().toString(),groups,users,Utils.localToUTCDateTime(Utils.getDatetime()));

        Log.d(TAG, "onValidationSucceeded: "+Utils.localToUTCDateTime(Utils.getDatetime()));


        showProgress("Creating case..");

        apiFactory.createCase(caseCreated)
                .flatMap(response -> {

                    String casecartedID = response.getData();

                    List<ChosenImage> images_path = adapter.getFiles();


                    List<MultipartBody.Part> parts = new ArrayList<>();


                    if(images_path != null && !images_path.isEmpty()) {
                        for (int i = 0; i < images_path.size(); i++) {
                            File file = new File(images_path.get(i).getThumbnailPath());
                            if (file.exists()) {
                                final MediaType MEDIA_TYPE = MediaType.parse(images_path.get(i).getMimeType());
                                parts.add(MultipartBody.Part.createFormData("my_images[]",
                                        file.getName(), RequestBody.create(MEDIA_TYPE, file)));

                            } else {
                                Log.d(TAG, "file not exist " + images_path.get(i).getOriginalPath());
                            }

                        }
                    }


                    return apiFactory.addCaseAttachments(casecartedID,parts);

                })
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(model-> {

                    if(model.status){
                        showSuccess(model.getMessage());
                        getFragmentHandlingActivity().replaceFragment(MyCasesFragment.newInstance());
                    }
                    else {
                        showError(model.getMessage());
                    }


                },throwable -> {
                    hideProgress();
                    throwable.printStackTrace();
                });





    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @OnClick(R.id.btn_create_case)
    public void CreateCaseClick() {
        validator.validate();
    }


    @OnItemSelected(R.id.spin_gender)
    public void spinnerItemSelected(Spinner spinner, int position) {
        genderPosition = position;
    }




    @OnItemSelected(R.id.referview)
    public void RefereItemSelected(Spinner spinner, int position) {
        if(++check > 1) {
            referrerItems.addView(getBubble(adapter1.getItem(position).name, adapter1.getItem(position)));
            if(adapter1.getItem(position).type.equalsIgnoreCase("group")){
                groups.add(Integer.valueOf(adapter1.getItem(position).id));
            }else {
                users.add(Integer.valueOf(adapter1.getItem(position).id));
            }
        }
    }

    public View getBubble(String name,GroupAndUser groupAndUser){
        LinearLayout linearLayout = (LinearLayout) getBubbleView();
        TextView view1 = (TextView) linearLayout.findViewById(R.id.textname);
        view1.setText(name);
        view1.setTag(groupAndUser);
        return linearLayout;
    }



    public View getBubbleView(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.view_bubble, referrerItems, false);
        v.setOnClickListener(v1 -> referrerItems.removeView(v1));


        return  v;
    }





}

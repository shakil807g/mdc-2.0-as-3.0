package com.mdconsults.healthprovider.cases;


import com.google.gson.JsonObject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;

import com.mdconsults.Events;
import com.mdconsults.R;
import com.mdconsults.base.BaseFragment;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.di.AppComponent;
import com.mdconsults.healthprovider.MainHealthProviderActivity;
import com.mdconsults.healthprovider.cases.adapter.MyCasesAdapter;
import com.mdconsults.util.RxBus;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.VerticalSpaceItemDecoration;
import com.mdconsults.widget.ActionBar;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;


public class MyCasesFragment extends BaseFragment {
    @BindView(R.id.main_list)
    RecyclerView mainList;
    @BindDimen(R.dimen.item_space_small)
    int listviewSpace;

    @Inject
    RxBus rxBus;
    @Inject
    ApiFactory apiFactory;

    @BindView(R.id.search_view)
    SearchView searchView;

    private MyCasesAdapter adapter;

    public MyCasesFragment() {
        // Required empty public constructor
    }

    public static MyCasesFragment newInstance() {
        MyCasesFragment fragment = new MyCasesFragment();
        return fragment;
    }




    @Override
    public void onStart() {
        super.onStart();
        rxBus.asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindUntilEvent(FragmentEvent.STOP))
                .subscribe(o -> {

                    if(o instanceof Events.Success){
                        showSuccess("Case Created");
                        getCasesForReffer();

                    }
                    if(o instanceof Events.Failed){
                        showError(((Events.Failed) o).getMessage());
                    }


                });



    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(MainHealthProviderActivity.isReviewer){
            showProgress("Loading");
            getCaseforReviwers();
        }else {
            getCasesForReffer();
        }
    }

    public void getCasesForReffer(){

        apiFactory.mycases(getUser().getId())
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(model->{

                    hideProgress();
                    adapter.swap(model.getData());

                }, Throwable::printStackTrace);




    }

    List<JsonObject> data = new ArrayList<>();

    public void getCaseforReviwers(){

        apiFactory.ReviewerCasesUser(getUser().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(model ->  {

                    data.addAll(model.getData());

                    apiFactory.ReviewerCasesGroups(getUser().getId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(model2 ->  {

                                data.addAll(model2.getData());
                                adapter.swap(data);
                                hideProgress();

                            } , t -> {
                                t.printStackTrace();
                                adapter.swap(data);

                            });

                    });

    }



    @Override
    public int getLayoutID() {
        return R.layout.fragment_my_cases;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initList();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.setQuery((newText != null) ? newText : null);
                return true;
            }
        });
    }

    private void initList() {
        adapter = new MyCasesAdapter(getContext());
        mainList.setAdapter(adapter);
        mainList.setHasFixedSize(true);
        mainList.setItemAnimator(new DefaultItemAnimator());
        mainList.addItemDecoration(new VerticalSpaceItemDecoration(listviewSpace));
        mainList.setNestedScrollingEnabled(false);
        mainList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.setItemClickListner(object -> {

                getFragmentHandlingActivity().addFragmentWithBackstack(CaseDetailFragment.newInstance(
                        object.get("record_id").getAsString()));

     });

    }





}

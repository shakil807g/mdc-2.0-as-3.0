package com.mdconsults.healthprovider.group;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.mdconsults.R;
import com.mdconsults.base.BaseFragment;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.di.AppComponent;
import com.mdconsults.util.RxUtil;
import com.mdconsults.widget.ActionBar;
import com.trello.rxlifecycle2.android.FragmentEvent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.Unbinder;


public class GroupDetailFragment extends BaseFragment {

    @Inject
    ApiFactory apiFactory;
    @BindView(R.id.txt_group_members)
    TextView txtGroupMembers;
    @BindView(R.id.txt_group_speciality)
    TextView txtGroupSpeciality;
    Unbinder unbinder;

    private String mGroupID;


    public GroupDetailFragment() {
    }


    public static GroupDetailFragment newInstance(String groupID) {
        Bundle args = new Bundle();
        GroupDetailFragment fragment = new GroupDetailFragment();
        args.putString("groupid", groupID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGroupID = getArguments().getString("groupid");
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showProgress("loading..");
        apiFactory.groupDetail(mGroupID)
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(model -> {
                    hideProgress();

                    try {

                        if (model != null && model.getStatus() && model.getData() != null) {
                            for (int i = 0; i < model.getData().size(); i++) {
                                txtGroupMembers.append("."+model.getData().get(i).get("name").getAsString()+"\n");
                            }
                            txtGroupSpeciality.append("."+model.getData().get(0).get("group_speciality").getAsString()+"\n");



                        }

                    }catch (Exception ex){

                    }


                });


    }

    @Override
    public int getLayoutID() {
        return R.layout.fragment_group_detail;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }



}

package com.mdconsults.healthprovider.group;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mdconsults.R;
import com.mdconsults.base.BaseDialogFragment;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.data.remote.model.CreateGroup;
import com.mdconsults.data.remote.model.Response;
import com.mdconsults.di.AppComponent;
import com.mdconsults.util.RxUtil;
import com.mdconsults.widget.MembersView;
import com.mdconsults.widget.SpecialityView;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by Shakil Karim on 4/8/17.
 */

public class NewGroupDailog extends BaseDialogFragment {


    @BindView(R.id.item_1)
    LinearLayout item1;
    @BindView(R.id.item_2)
    LinearLayout item2;
    @BindView(R.id.check_private)
    CheckedTextView checkPrivate;
    @BindView(R.id.check_public)
    CheckedTextView checkPublic;
    String groupType = "Public";
    @BindView(R.id.txt_group_name)
    EditText txtGroupName;
    @BindView(R.id.next_btn)
    Button nextbtn;
    @BindView(R.id.skillview)
    SpecialityView specialityView;
    @BindView(R.id.pre_btn)
    Button pre_btn;
    @Inject
    ApiFactory apiFactory;


    private static final String TAG = "NewGroupDailog";
    @BindView(R.id.view_member)
    MembersView viewMember;
    @BindView(R.id.btn_finish)
    Button btnFinish;

    static boolean mEditMembers;
    static boolean misView;
    static CreateGroup mCreateGroup;


    public static NewGroupDailog newInstance(boolean isEditMember,boolean isView,CreateGroup createGroup) {
        NewGroupDailog fragment = new NewGroupDailog();
        misView = isView;
        mCreateGroup = createGroup;
        mEditMembers = isEditMember;
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(!misView) {
            RxTextView.textChanges(txtGroupName)
                    .skip(1)
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .switchMap(string -> apiFactory.isGroupExists(string.toString()))
                    .compose(RxUtil.applySchedulers())
                    .subscribe(objectResponse -> {

                        Log.d(TAG, "onNext: " + objectResponse);

                        if (objectResponse.status) {
                            txtGroupName.setError(null);
                            // nextbtn.setVisibility(View.VISIBLE);
                        } else {
                            txtGroupName.setError("Group already exists!");
                            //nextbtn.setVisibility(View.GONE);
                        }

                    });
        }


    }

    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public int getLayoutID() {
        return R.layout.dailog_create_group;
    }


    @OnClick(R.id.next_btn)
    public void nextClicked() {

        if(TextUtils.isEmpty(txtGroupName.getText().toString())){
            showError("Group name is empty");
            return;
        }

        item1.setVisibility(View.GONE);
        item2.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.pre_btn)
    public void preClicked() {
        item1.setVisibility(View.VISIBLE);
        item2.setVisibility(View.GONE);
    }


    @OnClick({R.id.check_private, R.id.check_public})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.check_private:
                groupType = "Private";
                checkPrivate.setChecked(true);
                checkPublic.setChecked(false);
                break;
            case R.id.check_public:
                groupType = "Public";
                checkPrivate.setChecked(false);
                checkPublic.setChecked(true);
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(mEditMembers){
            item1.setVisibility(View.GONE);
            item2.setVisibility(View.VISIBLE);
            pre_btn.setVisibility(View.GONE);

        }


        if(misView){
            txtGroupName.setText(mCreateGroup.getName());

        }
    }

    @OnClick(R.id.btn_finish)
    public void onBtnFinish() {

        showProgress("Creating case");

        CreateGroup createGroup = new CreateGroup();
        createGroup.setName(txtGroupName.getText().toString());
        createGroup.setMembers(viewMember.getMembersId());
        createGroup.setPermission(groupType);
        createGroup.setColor(generateColor(new Random()));
        createGroup.setUserId(getUser().getId());
        createGroup.setGroup_diseases(specialityView.getSepicallitIds());

        if(misView){
            createGroup.setId(mCreateGroup.getId());
        }

        if(!misView) {
            apiFactory.createGroup(createGroup)
                    .compose(RxUtil.applySchedulers())
                    .subscribe(objectResponse -> {

                            if (objectResponse.status) {
                                hideProgress();
                                getFragmentHandlingActivity().replaceFragment(GroupFragment.newInstance());
                                dismiss();
                            } else {
                                showError(objectResponse.message);
                            }

                        });
        }else {
            if(!mEditMembers)
            {
                apiFactory.groupEdit(createGroup)
                        .compose(RxUtil.applySchedulers())
                        .subscribe(objectResponse ->{

                                if (objectResponse.status) {
                                    hideProgress();
                                    getFragmentHandlingActivity().replaceFragment(GroupFragment.newInstance());
                                    dismiss();
                                } else {
                                    showError(objectResponse.message);
                                }

                            });
            }else{
                apiFactory.groupEditAddMember(createGroup)
                        .compose(RxUtil.applySchedulers())
                        .subscribe(objectResponse-> {

                                if (objectResponse.status) {
                                    hideProgress();
                                    getFragmentHandlingActivity().replaceFragment(GroupFragment.newInstance());
                                    dismiss();
                                } else {
                                    showError(objectResponse.message);
                                }

                            });

            }


        }



    }


    private static String generateColor(Random r) {
        final char [] hex = { '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        char [] s = new char[7];
        int     n = r.nextInt(0x1000000);

        s[0] = '#';
        for (int i=1;i<7;i++) {
            s[i] = hex[n & 0xf];
            n >>= 4;
        }
        return new String(s);
    }

    @Override
    public void onValidationSucceeded() {

    }
}

package com.mdconsults.healthprovider.group;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mdconsults.R;
import com.mdconsults.base.BaseFragment;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.data.remote.model.CreateGroup;
import com.mdconsults.data.remote.model.Response;
import com.mdconsults.di.AppComponent;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.VerticalSpaceItemDecoration;
import com.mdconsults.widget.ActionBar;
import com.trello.rxlifecycle2.android.FragmentEvent;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by Shakil Karim on 4/8/17.
 */

public class GroupFragment extends BaseFragment {

    @BindView(R.id.main_list)
    RecyclerView mainList;
    @BindDimen(R.dimen.item_space_small)
    int listviewSpace;
    private GroupAdapter adapter;

    @Inject
    ApiFactory apiFactory;

    public static GroupFragment newInstance() {
        GroupFragment fragment = new GroupFragment();
        return fragment;
    }


    @Override
    public int getLayoutID() {
        return R.layout.fragment_group;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initList();
        showProgress("Loading");
        apiFactory.groups(getUser().getId())
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(createGroupResponse -> {
                        if(createGroupResponse.status){
                            hideProgress();
                            adapter.swap(createGroupResponse.data);
                        }
                        else {
                            showError(createGroupResponse.message);
                        }
                    });
    }

    private void initList() {
        adapter = new GroupAdapter(getActivity());
        mainList.setAdapter(adapter);
        mainList.setHasFixedSize(true);
        mainList.setItemAnimator(new DefaultItemAnimator());
        mainList.addItemDecoration(new VerticalSpaceItemDecoration(listviewSpace));
        mainList.setNestedScrollingEnabled(false);
        mainList.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter.setItemClickListner((view, item) -> {
            switch (view.getId()){
                case R.id.item_main:
                    break;
                case R.id.view_group:
                        getFragmentHandlingActivity()
                                .replaceFragmentWithBackstack(GroupDetailFragment.newInstance(item.getId()));
                    break;
                case R.id.edit_group:
                    getFragmentHandlingActivity().addDialog(NewGroupDailog.newInstance(false,true,item));
                    break;
                case R.id.add_member:
                    getFragmentHandlingActivity().addDialog(NewGroupDailog.newInstance(true,true,item));
                    break;
                case R.id.btn_delete:
                    showProgress("Deleting group..");
                    apiFactory.groupDelete(item)
                            .compose(RxUtil.applySchedulers())
                            .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                            .subscribe(model -> {
                                hideProgress();
                                if (model.status) {
                                    hideProgress();
                                    getFragmentHandlingActivity().replaceFragment(GroupFragment.newInstance());
                                } else {
                                    showError(model.message);
                                }

                            });
                    break;

            }


        });

//        adapter.setItemDelete(pos -> {
//
//
//
//
////            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
////                    .setTitleText("Are you sure?")
////                    .setContentText("Won't be able to recover this group!")
////                    .setConfirmText("Yes,delete it!")
////                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
////                        @Override
////                        public void onClick(SweetAlertDialog sDialog) {
////                            sDialog
////                                    .setTitleText("Deleted!")
////                                    .setContentText("Your group has been deleted!")
////                                    .setConfirmText("OK")
////                                    .setConfirmClickListener(null)
////                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
////
////                            adapter.mlist.remove(pos);
////                            adapter.notifyDataSetChanged();
////                        }
////                    })
////                    .show();
//
//
//        });

    }
}

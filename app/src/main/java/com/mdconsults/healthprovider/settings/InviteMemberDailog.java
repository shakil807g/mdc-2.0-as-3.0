package com.mdconsults.healthprovider.settings;

import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mdconsults.R;
import com.mdconsults.base.BaseDialogFragment;
import com.mdconsults.di.AppComponent;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Shakil Karim on 4/8/17.
 */

public class InviteMemberDailog extends BaseDialogFragment {

    @BindView(R.id.invite)
    Button invite;
    @BindView(R.id.textView33)
    TextView textView33;
    @BindView(R.id.editText5)
    EditText editText5;



    public static InviteMemberDailog newInstance() {

        // Bundle args = new Bundle();

        InviteMemberDailog fragment = new InviteMemberDailog();
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void provideInjection(AppComponent appComponent) {

    }

    @Override
    public int getLayoutID() {
        return R.layout.dailog_invite_member;
    }

    @Override
    public void onValidationSucceeded() {

    }

    @OnClick(R.id.invite)
    public void onViewClicked() {

        if(TextUtils.isEmpty(editText5.getText().toString())) {
            Toast.makeText(getActivity(), "Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }

        dismiss();

        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Member invited!!")
                .setConfirmText("ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();

    }
}

package com.mdconsults.healthprovider.appointment;


import com.google.gson.JsonObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mdconsults.R;
import com.mdconsults.base.BaseFragment;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.di.AppComponent;
import com.mdconsults.healthprovider.cases.CaseDetailFragment;
import com.mdconsults.util.AppPref;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.VerticalSpaceItemDecoration;
import com.mdconsults.widget.ActionBar;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;


public class HealthProviderAppointmentFragment extends BaseFragment {

    @BindView(R.id.main_list)
    RecyclerView mainList;
    @BindDimen(R.dimen.item_space_small)
    int listviewSpace;
    @Inject
    ApiFactory apiFactory;
    private HealthProviderAppointmentAdapter adapter;

    public HealthProviderAppointmentFragment() {
        // Required empty public constructor
    }

    public static HealthProviderAppointmentFragment newInstance() {
        HealthProviderAppointmentFragment fragment = new HealthProviderAppointmentFragment();
        return fragment;
    }


    @Override
    public int getLayoutID() {
        return R.layout.fragment_appointments;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initList();
        showProgress("Loading");

        apiFactory.getAppointmentforHealthProvider(getUser().getId())
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(model -> {

                    if(model.status){
                        hideProgress();
                        adapter.swap(model.getData());
                    }
                    else {
                        showError(model.getMessage());
                    }

                },throwable -> showError(throwable.getMessage()));
    }

    private void initList() {
        adapter = new HealthProviderAppointmentAdapter(getContext());
        mainList.setAdapter(adapter);
        mainList.setHasFixedSize(true);
        mainList.setItemAnimator(new DefaultItemAnimator());
        mainList.addItemDecoration(new VerticalSpaceItemDecoration(listviewSpace));
        mainList.setNestedScrollingEnabled(false);
        mainList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.setItemClickListner(item -> {

            getPref().put(AppPref.Key.KEY_RECORD_ID,item.get("record_id").getAsString());

            getFragmentHandlingActivity()
                    .addFragmentWithBackstack(CaseDetailFragment.
                            newInstance(item.get("record_id").getAsString()));


        });

        adapter.setVideoClickListner(item->{

            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://mdconsults.org/live/"+item.get("room_url").getAsString()));
            startActivity(intent);
           // getFragmentHandlingActivity().addFragmentWithBackstack(VideoChatFragment.newInstance(item.get("record_id").getAsString()));

        });


    }
}

package com.mdconsults.data.model;

/**
 * Created by Shakil Karim on 5/16/17.
 */

public class GroupAndUser {

    public String id;
    public String type;
    public String name;

    public GroupAndUser(String id, String type, String name) {
        this.id = id;
        this.type = type;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

package com.mdconsults.data.local.Doa;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;


import com.mdconsults.data.model.MainAppointment;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by Shakil Karim on 7/1/17.
 */


@Dao
public interface MainAppointmentDoa {

    @Query("SELECT * FROM MainAppointment WHERE patient_id = :patient_id")
    Maybe<List<MainAppointment>> getMainAppointment(String patient_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<MainAppointment> mainAppointments);

    @Query("DELETE FROM MainAppointment WHERE patient_id = :patient_id")
    void delete(String patient_id);



}

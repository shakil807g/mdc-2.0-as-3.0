package com.mdconsults.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.mdconsults.data.local.Doa.MainAppointmentDetailDoa;
import com.mdconsults.data.local.Doa.MainAppointmentDetailImageDoa;
import com.mdconsults.data.local.Doa.MainAppointmentDoa;
import com.mdconsults.data.model.MainAppointment;
import com.mdconsults.data.model.User;
import com.mdconsults.data.remote.model.MainAppointmentDetail;
import com.mdconsults.data.remote.model.MainAppointmentDetailImage;

/**
 * Created by Shakil Karim on 7/1/17.
 */
@Database(entities = {
        MainAppointment.class,
        MainAppointmentDetail.class,
        MainAppointmentDetailImage.class
        }, version = 1)
public abstract class MDCDatabase extends RoomDatabase {

//    private static MDCDatabase INSTANCE;
//
//
//    public static MDCDatabase getInstance(Context context) {
//        if (INSTANCE == null) {
//            synchronized (MDCDatabase.class) {
//                if (INSTANCE == null) {
//                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
//                            MDCDatabase.class, "mdc.db")
//                            .build();
//                }
//            }
//        }
//        return INSTANCE;
//    }



    public abstract MainAppointmentDoa mainAppointmentDoa();

    public abstract MainAppointmentDetailDoa mainAppointmentDetailDoa();

    public abstract MainAppointmentDetailImageDoa mainAppointmentDetailImageDoa();

}

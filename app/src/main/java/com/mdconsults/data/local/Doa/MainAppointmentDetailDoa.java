package com.mdconsults.data.local.Doa;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mdconsults.data.model.MainAppointment;
import com.mdconsults.data.remote.model.MainAppointmentDetail;

import java.util.List;

import javax.inject.Singleton;

/**
 * Created by Shakil Karim on 7/2/17.
 */
@Dao
public interface MainAppointmentDetailDoa {

    @Query("SELECT * FROM MainAppointmentDetail WHERE id = :id")
    List<MainAppointmentDetail> getMainAppointmentDetail(String id);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<MainAppointmentDetail> mainAppointments);

    @Query("DELETE FROM MainAppointmentDetail WHERE id = :id")
    void delete(String id);

}

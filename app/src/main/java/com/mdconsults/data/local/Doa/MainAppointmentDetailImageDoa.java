package com.mdconsults.data.local.Doa;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mdconsults.data.remote.model.MainAppointmentDetail;
import com.mdconsults.data.remote.model.MainAppointmentDetailImage;

import java.util.List;

/**
 * Created by Shakil Karim on 7/2/17.
 */
@Dao
public interface MainAppointmentDetailImageDoa {

    @Query("SELECT * FROM MainAppointmentDetailImage WHERE id = :id")
    List<MainAppointmentDetailImage> getImageDetails(String id);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<MainAppointmentDetailImage> mainAppointments);

    @Query("DELETE FROM MainAppointmentDetailImage WHERE id = :id")
    void delete(String id);
}

package com.mdconsults.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.mdconsults.data.model.Farmer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shakil Karim on 4/9/17.
 */

public class FarmersListResponse {
    @SerializedName("farmer")
    @Expose
    public List<Farmer> farmer = new ArrayList<Farmer>();
    @SerializedName("message")
    @Expose
    public String message;
}

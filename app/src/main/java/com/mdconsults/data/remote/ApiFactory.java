package com.mdconsults.data.remote;

import com.google.gson.JsonObject;

import com.mdconsults.data.remote.model.AppointmentDoctor;
import com.mdconsults.data.remote.model.CareGiver;
import com.mdconsults.data.remote.model.City;
import com.mdconsults.data.remote.model.Country;
import com.mdconsults.data.remote.model.CreateGroup;
import com.mdconsults.data.remote.model.DoctorSchedule;
import com.mdconsults.data.remote.model.GroupItem;
import com.mdconsults.data.remote.model.Location;
import com.mdconsults.data.remote.model.PreviousSchedule;
import com.mdconsults.data.remote.model.SingleResponse;
import com.mdconsults.data.remote.model.States;
import com.mdconsults.data.remote.model.TimeSlots;
import com.mdconsults.data.remote.request_model.CaseCreated;
import com.mdconsults.data.remote.request_model.Login;
import com.mdconsults.data.model.MainAppointment;
import com.mdconsults.data.remote.model.MainAppointmentDetail;
import com.mdconsults.data.remote.model.MainAppointmentDetailImage;
import com.mdconsults.data.remote.model.Response;
import com.mdconsults.data.model.User;
import com.mdconsults.data.remote.model.Schedule;
import com.mdconsults.data.remote.model.UserNames;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;


/**
 * Created by Shakil Karim on 4/9/17.
 */

public interface ApiFactory {


    @FormUrlEncoded
    @POST("GroupDetail")
    Observable<Response<List<JsonObject>>> groupDetail(@Field("group_id") String group_id);


    @POST("groupDelete")
    Observable<Response<List<Object>>> groupDelete(@Body CreateGroup createGroup);


    @POST("groupEdit")
    Observable<Response<List<Object>>> groupEdit(@Body CreateGroup createGroup);

    @POST("groupEditAddMember")
    Observable<Response<List<Object>>> groupEditAddMember(@Body CreateGroup createGroup);

    @FormUrlEncoded
    @POST("groups")
    Observable<Response<List<CreateGroup>>> groups(@Field("user_id") String user_id);

    @GET("users")
    Observable<Response<List<User>>> getUsers();


    @FormUrlEncoded
    @POST("filterUsername")
    Observable<Response<List<UserNames>>> filterUsername(@Field("name") String name,@Field("licenses_id") String licenses_id);

    @FormUrlEncoded
    @POST("isGroupExists")
    Observable<Response<List<Object>>> isGroupExists(@Field("group_name") String group_name);



    @GET("locations")
    Observable<Response<List<Location>>> locations();


    @GET("usersNames/{licenses_id}")
    Observable<Response<List<UserNames>>> usersNames(@Path("licenses_id") String licenses_id);



    @POST("createGroup")
    Observable<Response<List<Object>>> createGroup(@Body CreateGroup createGroup);


    @POST("login")
    Observable<Response<List<User>>> login(@Body Login login);

    @POST("signup")
    Observable<Response<List<User>>> signup(@Body User user);

    @POST("create_schedule")
    Observable<Response<List<Object>>> createSchedule(@Body Schedule user);


    @FormUrlEncoded
    @POST("usersforappoint")
    Observable<Response<List<AppointmentDoctor>>> getUserforAppoint(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("doctorslots")
    Observable<Response<List<DoctorSchedule>>> getDoctorTimeSlots(@Field("doc_id") String doc_id);


    @FormUrlEncoded
    @POST("scheduleforDay")
    Observable<Response<List<TimeSlots>>> scheduleforDay(@Field("schedule_id") String schedule_id, @Field("day") String day);


    @FormUrlEncoded
    @POST("getAppointmentfordoctors")
    Flowable<Response<List<MainAppointment>>> getAppointmentfordoctors(@Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST("getAppointmentforHealthProvider")
    Observable<Response<List<JsonObject>>> getAppointmentforHealthProvider(@Field("user_id") String doctor_id);


    @FormUrlEncoded
    @POST("getAppointmentDetails")
    Flowable<Response<List<MainAppointmentDetail>>> getAppointmentDetails(@Field("record_id") String record_id);

    @FormUrlEncoded
    @POST("getAppointmentDetailsImages")
    Flowable<Response<List<MainAppointmentDetailImage>>> getAppointmentDetailsImages(@Field("record_id") String record_id);


    @FormUrlEncoded
    @POST("mycases")
    Observable<Response<List<JsonObject>>> mycases(@Field("referer_id") String referer_id);


    @FormUrlEncoded
    @POST("mycasesDetails")
    Observable<Response<JsonObject>> mycasesDetails(@Field("case_id") String case_id);


    @GET("countries")
    Observable<Response<List<Country>>> countries();


    @GET("states/{country_id}")
    Observable<Response<List<States>>> states(@Path("country_id") int country_id);


    @GET("cities/{state_id}")
    Observable<Response<List<City>>> cities(@Path("state_id") int state_id);

    @FormUrlEncoded
    @POST("previousSchedules")
    Observable<Response<List<PreviousSchedule>>> previousSchedules(@Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST("userLocation")
    Observable<Response<List<Object>>> userLocation(@Field("user_id") String user_id,
                                              @Field("country_id") String country_id,
                                              @Field("state_id") String state_id,
                                              @Field("city_id") String city_id,
                                              @Field("area") String area);

    @FormUrlEncoded
    @POST("addCareGiver")
    Observable<Response<List<Object>>> addCareGiver(@Field("user_id") String user_id,
                                              @Field("name") String name,
                                              @Field("email") String email,
                                              @Field("mobile") String mobile,
                                              @Field("relation") String relation,
                                              @Field("send_to") String send_to);

    @FormUrlEncoded
    @POST("getCareGiver")
    Observable<Response<List<CareGiver>>> getCareGiver(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("groupByUsername")
    Observable<Response<List<GroupItem>>> groupByUsername(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("createAppointmentfromMycase")
    Observable<Response<List<GroupItem>>> createAppointmentfromMycase(
                            @Field("case_id") String case_id,
                            @Field("doctor_id") String doctor_id,
                            @Field("referer_id") String referer_id,
                            @Field("slot_id") String slot_id,
                            @Field("patient_id") String patient_id,
                            @Field("date") String date,
                            @Field("create_at") String create_at,
                            @Field("code") String code);


    @POST("createCase")
    Observable<SingleResponse<String>> createCase(@Body CaseCreated caseCreated);


    @FormUrlEncoded
    @POST("ReviewerCasesUser")
    Observable<Response<List<JsonObject>>> ReviewerCasesUser(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("ReviewerCasesGroups")
    Observable<Response<List<JsonObject>>> ReviewerCasesGroups(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("addCommentsInCase")
    Observable<Response<List<JsonObject>>> addCommentsInCase(
            @Field("sender_id") String sender_id,
            @Field("comment") String comment,
            @Field("case_id") String case_id,
            @Field("created_at") String created_at);



    @Multipart
    @POST("createAppointment")
    Observable<Response<List<String>>> createAppointment(
            @Part("description") String description,
            @Part("referer_id") String referer_id,
            @Part("patient_id") String patient_id,
            @Part("name") String name,
            @Part("lname") String lname,
            @Part("gender") String gender,
            @Part("y_age") String y_age,
            @Part("month_age") String month_age,
            @Part("m_age") String m_age,
            @Part("slot_id") String slot_id,
            @Part("date") String date,
            @Part("created_date") String created_date,
            @Part("booked_as") String booked_as,
            @Part("unique_key") String unique_key,
            @Part List<MultipartBody.Part> files);

    @Multipart
    @POST("addCaseAttachments")
    Observable<Response<List<String>>> addCaseAttachments(
            @Part("case_id") String case_id,
            @Part List<MultipartBody.Part> files);


    @Multipart
    @POST("updateProfilePic")
    Observable<Response<List<String>>> updateProfilePic(
            @Part("user_id") String user_id,
            @Part("email") String email,
            @Part("name") String name,
            @Part("username") String username,
            @Part("speciality") String speciality,
            @Part("gender") String gender,
            @Part("password") String password,
            @Part("country") String country,
            @Part("state") String state,
            @Part("city") String city,
            @Part MultipartBody.Part file);


}

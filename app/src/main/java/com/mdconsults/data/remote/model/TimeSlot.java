package com.mdconsults.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shakil Karim on 4/16/17.
 */

public class TimeSlot {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("doctor_id")
    @Expose
    public String doctorId;
    @SerializedName("start_date")
    @Expose
    public String startDate;
    @SerializedName("end_date")
    @Expose
    public String endDate;
    @SerializedName("slot_price")
    @Expose
    public String slotPrice;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("schedule_id")
    @Expose
    public String scheduleId;
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("open")
    @Expose
    public String open;
    @SerializedName("start_time")
    @Expose
    public String startTime;
    @SerializedName("end_time")
    @Expose
    public String endTime;

}

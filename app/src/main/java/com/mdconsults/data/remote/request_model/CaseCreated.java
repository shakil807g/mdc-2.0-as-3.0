package com.mdconsults.data.remote.request_model;

import java.util.List;

/**
 * Created by Shakil Karim on 5/20/17.
 */

public class CaseCreated {

    public String case_code;
    public String fname;
    public String lname;
    public String gender;
    public String y_age;
    public String m_age;
    public String disease_id;
    public String location_id;
    public String history;
    public String question;
    public String city;
    public String form_id;
    public String patient_id;
    public String referer_id;
    public String data;
    public String comment;
    List<Integer> groups;
    List<Integer> users;
    public String datetime;

    public CaseCreated(String case_code, String fname, String lname, String gender, String y_age, String m_age, String disease_id, String location_id, String history, String question, String city, String form_id, String patient_id, String referer_id, String data, String comment, List<Integer> groups, List<Integer> users, String datetime) {
        this.case_code = case_code;
        this.fname = fname;
        this.lname = lname;
        this.gender = gender;
        this.y_age = y_age;
        this.m_age = m_age;
        this.disease_id = disease_id;
        this.location_id = location_id;
        this.history = history;
        this.question = question;
        this.city = city;
        this.form_id = form_id;
        this.patient_id = patient_id;
        this.referer_id = referer_id;
        this.data = data;
        this.comment = comment;
        this.groups = groups;
        this.users = users;
        this.datetime = datetime;
    }
}

package com.mdconsults.data.remote.request_model;

/**
 * Created by Shakil Karim on 4/11/17.
 */

public class Login {
    public String username;
    public String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }
}

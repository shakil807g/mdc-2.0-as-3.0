package com.mdconsults.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Shakil Karim on 4/19/17.
 */
@Entity
public class MainAppointmentDetailImage {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("record_id")
    @Expose
    public String recordId;
    @SerializedName("image")
    @Expose
    public String image;

}

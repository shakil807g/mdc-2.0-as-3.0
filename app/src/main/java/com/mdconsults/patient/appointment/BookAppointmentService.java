package com.mdconsults.patient.appointment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;


import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.mdconsults.Events;
import com.mdconsults.R;
import com.mdconsults.app.MyApplication;
import com.mdconsults.data.model.User;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.util.AppPref;
import com.mdconsults.util.ProgressRequestBody;
import com.mdconsults.util.RxBus;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class BookAppointmentService extends Service {
    private static final String TAG = "ImagesUploadService";

    int NOTIFICATION_ID = 1232312;

    NotificationManager mNotifyManager;

    List<ChosenImage> images_path;
    String slot_id;
    String doc_id;
    String description;

    @Inject
    RxBus rxBus;
    @Inject
    ApiFactory apiFactory;


    private String date;

    public BookAppointmentService() {
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        images_path = AppPref.getInstance(this).getImageList(AppPref.Key.STEP_LIST_IMAGES);
        slot_id = AppPref.getInstance(this).getString(AppPref.Key.STEP_SLOT_ID);
        doc_id = AppPref.getInstance(this).getString(AppPref.Key.STEP_DOC_ID);
        description = AppPref.getInstance(this).getString(AppPref.Key.STEP_DISCRIPTION);
        date = AppPref.getInstance(this).getString(AppPref.Key.STEP_DATE);

         UploadtoServer();

        return START_NOT_STICKY;
    }



    @Override
    public void onCreate() {
        super.onCreate();

        mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        ((MyApplication)getApplication()).getAppComponent().inject(this);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return  null;
    }




    public void UploadtoServer(){


        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.icon_camera);
        builder.setContentTitle("MDConsults 2.0");
        builder.setContentText("Image uploading....");
        builder.setProgress(100,0,true);
        startForeground(NOTIFICATION_ID, builder.build());
        mNotifyManager.notify(NOTIFICATION_ID,builder.build());

        User user = AppPref.getInstance(this).getUser(AppPref.Key.USER_LOGIN);



        List<MultipartBody.Part> parts = new ArrayList<>();


           if(images_path != null && !images_path.isEmpty()) {
            for (int i = 0; i < images_path.size(); i++) {
                File file = new File(images_path.get(i).getThumbnailPath());
                if (file.exists()) {
                    final MediaType MEDIA_TYPE = MediaType.parse(images_path.get(i).getMimeType());
                    parts.add(MultipartBody.Part.createFormData("my_images[]",
                            file.getName(), RequestBody.create(MEDIA_TYPE, file)));

                } else {
                    Log.d(TAG, "file not exist " + images_path.get(i).getOriginalPath());
                }

            }
        }

        rxBus.send(new Events.Start());

        apiFactory.createAppointment(
                TextUtils.isEmpty(description) ? "" : description,
                doc_id,
                user.id,user.name,"",user.gender,"","","",slot_id,date,Utils.localToUTCDateTime(Utils.getDatetime()),"patient",UUID.randomUUID().toString(),parts)
                .compose(RxUtil.applySchedulers())
                .subscribe(model -> {

                    AppPref.getInstance(this).remove(AppPref.Key.STEP_LIST_IMAGES,
                            AppPref.Key.STEP_SLOT_ID,AppPref.Key.STEP_DOC_ID,
                            AppPref.Key.STEP_DISCRIPTION,AppPref.Key.STEP_DATE);


                    if(model.status){
                        rxBus.send(new Events.Success());
                        stopForeground(true);
                        stopSelf();
                    }
                    else {
                        rxBus.send(new Events.Failed(model.getMessage()));
                        stopForeground(true);
                        stopSelf();
                    }

                },throwable -> {
                    rxBus.send(new Events.Failed(throwable.getMessage()));
                    stopForeground(true);
                    stopSelf();
                    throwable.printStackTrace();

                });





    }


    ProgressRequestBody.Listener listener = new ProgressRequestBody.Listener() {
        @Override
        public void onProgress(int progress) {
            Log.d(TAG, "onProgress: "+progress);
           // rxBus.send(new Events.Progress(progress));
        }
    };






}

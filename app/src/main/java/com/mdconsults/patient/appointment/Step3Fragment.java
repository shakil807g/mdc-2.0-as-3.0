package com.mdconsults.patient.appointment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.mdconsults.R;
import com.mdconsults.base.BaseFragment;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.data.remote.model.AppointmentDoctor;
import com.mdconsults.di.AppComponent;
import com.mdconsults.util.AppPref;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.TextWatcherAdapter;
import com.mdconsults.util.VerticalSpaceItemDecoration;
import com.mdconsults.widget.ActionBar;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;

/**
 * Created by Shakil Karim on 4/16/17.
 */

public class Step3Fragment extends BaseFragment implements BlockingStep {

    @BindView(R.id.doc_list)
    RecyclerView docList;
    @BindDimen(R.dimen.item_space_small)
    int listviewSpace;
    @BindView(R.id.search_by_spec)
    EditText searchBySpec;
    @BindView(R.id.search_by_doc_name)
    EditText search_by_doc_name;
    @BindView(R.id.search_by_location)
    EditText search_by_location;
    private DoctorsAppointmentAdapter doctorAdapter;

    @Inject
    ApiFactory apiFactory;

    private static final String TAG = "Step3Fragment";

    @Override
    public int getLayoutID() {
        return R.layout.fragment_step3;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        initDocList();
        searchBySpec.addTextChangedListener(mTextWatcher);
        search_by_doc_name.addTextChangedListener(mNameTextWatcher);
        search_by_location.addTextChangedListener(mLocationTextWatcher);
    }


    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }

    private void initDocList() {
        doctorAdapter = new DoctorsAppointmentAdapter();
        docList.setAdapter(doctorAdapter);
        docList.setHasFixedSize(true);
        docList.setItemAnimator(new DefaultItemAnimator());
        docList.addItemDecoration(new VerticalSpaceItemDecoration(listviewSpace));
        docList.setLayoutManager(new LinearLayoutManager(getContext()));
        doctorAdapter.setItemClickListener((doctor) -> {


            AppPref.getInstance(getActivity()).put(AppPref.Key.STEP_DOC_ID, doctor.id);
            ((PatientAppointmentFragment) getParentFragment()).stepperLayout.onTabClicked(3);


        });
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        callback.goToNextStep();
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        showProgress("Loading");
        apiFactory.getUserforAppoint(getUser().getId())
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.PAUSE))
                .subscribe(model ->{
                    hideProgress();
                    doctorAdapter.swap((List<AppointmentDoctor>) model.getData());

                },t-> showError(t.getMessage()));


    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }


    private final TextWatcher mTextWatcher = new TextWatcherAdapter() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            doctorAdapter.setQuery(s != null ? s.toString() : null);
        }
    };

    private final TextWatcher mNameTextWatcher = new TextWatcherAdapter() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            doctorAdapter.setNameQuery(s != null ? s.toString() : null);
        }
    };

    private final TextWatcher mLocationTextWatcher = new TextWatcherAdapter() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            doctorAdapter.setLocationQuery(s != null ? s.toString() : null);
        }
    };

}

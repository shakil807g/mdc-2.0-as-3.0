package com.mdconsults.patient.dashboard;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mdconsults.Events;
import com.mdconsults.R;
import com.mdconsults.data.MDCRepository;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.base.BaseFragment;
import com.mdconsults.di.AppComponent;
import com.mdconsults.patient.PatientViewModel;
import com.mdconsults.patient.dashboard.adapter.DBMainAppointmentAdapter;
import com.mdconsults.util.AppPref;
import com.mdconsults.util.RxBus;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.VerticalSpaceItemDecoration;
import com.mdconsults.widget.ActionBar;
import com.trello.rxlifecycle2.android.FragmentEvent;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;


public class PatientMainFragment extends BaseFragment {


    @BindView(R.id.main_list)
    RecyclerView mainList;
    @BindDimen(R.dimen.item_space_small)
    int listviewSpace;
    private static final String TAG = "PatientMainFragment";
    @Inject
    ApiFactory apiFactory;
    @Inject
    RxBus rxBus;
    @BindView(R.id.empty_view)
    TextView emptyView;
    @Inject
    MDCRepository repository;


    private DBMainAppointmentAdapter mainAppointmentAdapter;
    private PatientViewModel patientViewModel;


    public PatientMainFragment() {
        // Required empty public constructor
    }

    public static PatientMainFragment newInstance() {
        PatientMainFragment fragment = new PatientMainFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        patientViewModel = ViewModelProviders.of(getActivity()).get(PatientViewModel.class);
    }

    @Override
    public void provideInjection(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initList();

    }

    private void initList() {

        mainAppointmentAdapter = new DBMainAppointmentAdapter(getContext());
        mainList.setAdapter(mainAppointmentAdapter);
        mainList.setHasFixedSize(true);
        mainList.setItemAnimator(new DefaultItemAnimator());
        mainList.addItemDecoration(new VerticalSpaceItemDecoration(listviewSpace));
        mainList.setNestedScrollingEnabled(false);
        mainList.setLayoutManager(new LinearLayoutManager(getContext()));

        mainAppointmentAdapter.setItemClickListner(item -> {
            getPref().put(AppPref.Key.KEY_RECORD_ID, item.record_id);
            getFragmentHandlingActivity().addFragmentWithBackstack(AppointmentDetailFragment.newInstance(item.start_time,item.end_time,item.date));
        });
        mainAppointmentAdapter.setVideoClickListner(video -> {

            Log.d(TAG, "initList: " + video);

            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://mdconsults.org/live/"+video));
            startActivity(intent);
           // getFragmentHandlingActivity().addFragmentWithBackstack(VideoChatFragment.newInstance(video,false));

        });

    }

    @Override
    public void onStart() {
        super.onStart();
        rxBus.asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindUntilEvent(FragmentEvent.STOP))
                .subscribe(o -> {

                    if (o instanceof Events.Start) {
                        // progressContainer.setVisibility(View.VISIBLE);
                        showProgress("Booking an appointment...");
                    }

                    if (o instanceof Events.Progress) {
                        int pro = ((Events.Progress) o).getProgress();
                        //progressBar.setProgress(pro);
                        //progressText.setText(pro+"%");
                    }

                    if (o instanceof Events.Success) {
                        //progressContainer.setVisibility(View.GONE);
                        showSuccess("Appointment booked successfully");
                        getAppointments();
                    }
                    if (o instanceof Events.Failed) {
                        showError(((Events.Failed) o).getMessage());
                    }

                });





    }

    @Override
    public int getLayoutID() {
        return R.layout.fragment_patient_main;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getAppointments();

    }

    @Override
    public void onDestroyView() {
        mainList.setAdapter(null);
        super.onDestroyView();

    }


    public void getAppointments() {
        showProgress("Loading");

        patientViewModel.getMainAppointments(getUser().id)
                .compose(RxUtil.applySchedulersFlow())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(model -> {
                    if(model.isEmpty()){
                        emptyView.setVisibility(View.VISIBLE);
                    }else {
                        emptyView.setVisibility(View.GONE);
                    }

                    Log.d(TAG, "getAppointments: ");
                    hideProgress();

                    mainAppointmentAdapter.updateData(model);
                },t -> {
                    showError(t.getMessage());
                    Log.d(TAG, "error getAppointments: ");

                });




    }



}

package com.mdconsults.patient.dashboard.adapter;

import android.content.Context;
import com.mdconsults.R;
import com.mdconsults.data.model.MainAppointment;
import com.mdconsults.databinding.ItemDashboardBinding;
import com.mdconsults.di.AppModule;
import com.mdconsults.util.MyBaseAdapter;
import com.mdconsults.util.Utils;
import com.squareup.picasso.Picasso;

import static android.support.v7.widget.RecyclerView.NO_POSITION;

/**
 * Created by Shakil Karim on 6/2/17.
 */

public class DBMainAppointmentAdapter extends MyBaseAdapter<MainAppointment,ItemDashboardBinding> {

    private VideoClickListener videoClickListener;


    public DBMainAppointmentAdapter(Context context) {
        super(context);
    }



    public void setVideoClickListner(VideoClickListener videoClickListner) {
        this.videoClickListener = videoClickListner;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return R.layout.item_dashboard;
    }

    @Override
    protected void bindData(ItemDashboardBinding dataBinding, MainAppointment item, int position) {

        dataBinding.docName.setText(item.name);
        dataBinding.date.setText(Utils.formatDate(item.date));
        dataBinding.time.setText(Utils.format12HourTime(Utils.UTCTolocal(item.start_time)));

        Picasso.with(getContext()).load(AppModule.profileimage+item.image)
                .placeholder(R.drawable.image_placeholder).into(dataBinding.profileImage);

        dataBinding.getRoot().setOnClickListener(v -> {
            if (position != NO_POSITION && itemClickListner !=null) {
                itemClickListner.onItemClick(item);
            }
        });

        dataBinding.videoIcon.setOnClickListener(v -> {
            if (position != NO_POSITION) {
                if (videoClickListener != null) {
                    videoClickListener.onVideoIconClick(item.room_url);
                }
            }

        });


    }



    public interface VideoClickListener{
        public void onVideoIconClick(String url);
    }






}

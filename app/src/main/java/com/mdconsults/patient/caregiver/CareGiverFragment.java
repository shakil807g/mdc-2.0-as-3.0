package com.mdconsults.patient.caregiver;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mdconsults.R;
import com.mdconsults.base.BaseDialogFragment;
import com.mdconsults.base.BaseFragment;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.data.remote.model.CareGiver;
import com.mdconsults.di.AppComponent;
import com.mdconsults.util.RxUtil;
import com.mdconsults.util.VerticalSpaceItemDecoration;
import com.mdconsults.widget.ActionBar;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CareGiverFragment extends BaseFragment implements BaseDialogFragment.DialogClickListener {


    @BindView(R.id.care_giver_list)
    RecyclerView careGiverList;
    @BindView(R.id.add_care_fab)
    FloatingActionButton addCareFab;
    @BindDimen(R.dimen.item_space_small)
    int listviewSpace;
    Unbinder unbinder;

    @Inject
    ApiFactory apiFactory;
    private CareGiverAdapter adapter;

    public CareGiverFragment() {
        // Required empty public constructor
    }


    public static CareGiverFragment newInstance() {
        CareGiverFragment fragment = new CareGiverFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public void onStart() {
        super.onStart();

        initList();

        showProgress("Loading");
        apiFactory.getCareGiver(getUser().getId())
                .compose(RxUtil.applySchedulers())
                .compose(bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe(model -> {
                    if(model.status){
                        hideProgress();
                        adapter.swap((List<CareGiver>) model.getData());
                    }else {
                        showError(model.getMessage());
                    }

                },throwable -> showError(throwable.getMessage()));
    }

    private void initList() {
        adapter = new CareGiverAdapter(getContext());
        careGiverList.setAdapter(adapter);
        careGiverList.setHasFixedSize(true);
        careGiverList.setItemAnimator(new DefaultItemAnimator());
        careGiverList.addItemDecoration(new VerticalSpaceItemDecoration(listviewSpace));
        careGiverList.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    @Override
    public int getLayoutID() {
        return R.layout.fragment_care_giver;
    }

    @Override
    public void getActionBar(ActionBar actionBar) {

    }

    @Override
    public void provideInjection(AppComponent appComponent) {
    appComponent.inject(this);
    }

    @OnClick(R.id.add_care_fab)
    public void careGiverClick() {
        getFragmentHandlingActivity().addDialog(this,AddCareGiverDailog.newInstance());
    }


    @Override
    public void onYesClick() {
        showProgress("Loading");
        apiFactory.getCareGiver(getUser().getId())
                .compose(RxUtil.applySchedulers())
                .subscribe(model -> {
                    if(model.status) {
                        hideProgress();
                        adapter.swap(model.getData());
                    }else {
                        showError(model.getMessage());
                    }

                },throwable -> showError(throwable.getMessage()));
    }

    @Override
    public void onNoClick() {

    }
}

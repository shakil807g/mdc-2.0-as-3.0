
package android.databinding;
import com.mdconsults.BR;
@javax.annotation.Generated("Android Data Binding")
class DataBinderMapper  {
    final static int TARGET_MIN_SDK = 16;
    public DataBinderMapper() {
    }
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case com.mdconsults.R.layout.item_dashboard:
                    return com.mdconsults.databinding.ItemDashboardBinding.bind(view, bindingComponent);
                case com.mdconsults.R.layout.activity_login:
                    return com.mdconsults.databinding.ActivityLoginBinding.bind(view, bindingComponent);
                case com.mdconsults.R.layout.activity_user_profile:
                    return com.mdconsults.databinding.ActivityUserProfileBinding.bind(view, bindingComponent);
                case com.mdconsults.R.layout.fragment_group_detail:
                    return com.mdconsults.databinding.FragmentGroupDetailBinding.bind(view, bindingComponent);
                case com.mdconsults.R.layout.dailog_invite_member:
                    return com.mdconsults.databinding.DailogInviteMemberBinding.bind(view, bindingComponent);
                case com.mdconsults.R.layout.activity_setting:
                    return com.mdconsults.databinding.ActivitySettingBinding.bind(view, bindingComponent);
        }
        return null;
    }
    android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case -718621090: {
                if(tag.equals("layout/item_dashboard_0")) {
                    return com.mdconsults.R.layout.item_dashboard;
                }
                break;
            }
            case -237232145: {
                if(tag.equals("layout/activity_login_0")) {
                    return com.mdconsults.R.layout.activity_login;
                }
                break;
            }
            case -1980586255: {
                if(tag.equals("layout/activity_user_profile_0")) {
                    return com.mdconsults.R.layout.activity_user_profile;
                }
                break;
            }
            case 41674604: {
                if(tag.equals("layout/fragment_group_detail_0")) {
                    return com.mdconsults.R.layout.fragment_group_detail;
                }
                break;
            }
            case -10668353: {
                if(tag.equals("layout/dailog_invite_member_0")) {
                    return com.mdconsults.R.layout.dailog_invite_member;
                }
                break;
            }
            case -1398886442: {
                if(tag.equals("layout/activity_setting_0")) {
                    return com.mdconsults.R.layout.activity_setting;
                }
                break;
            }
        }
        return 0;
    }
    String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"
            ,"user"};
    }
}
// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import com.mdconsults.widget.ActionBar;
import com.mdconsults.widget.VectorDrawableTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainHealthProviderActivity_ViewBinding implements Unbinder {
  private MainHealthProviderActivity target;

  private View view2131296672;

  private View view2131296628;

  private View view2131296635;

  @UiThread
  public MainHealthProviderActivity_ViewBinding(MainHealthProviderActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainHealthProviderActivity_ViewBinding(final MainHealthProviderActivity target, View source) {
    this.target = target;

    View view;
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
    target.darwerList = Utils.findRequiredViewAsType(source, R.id.darwerList, "field 'darwerList'", ListView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", ActionBar.class);
    target.txtRole = Utils.findRequiredViewAsType(source, R.id.txt_role, "field 'txtRole'", VectorDrawableTextView.class);
    target.username = Utils.findRequiredViewAsType(source, R.id.username, "field 'username'", TextView.class);
    target.drawerPic = Utils.findRequiredViewAsType(source, R.id.drawer_pic, "field 'drawerPic'", CircleImageView.class);
    view = Utils.findRequiredView(source, R.id.switcher, "method 'switerClick'");
    view2131296672 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.switerClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.setting, "method 'setting'");
    view2131296628 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.setting();
      }
    });
    view = Utils.findRequiredView(source, R.id.signout, "method 'signOut'");
    view2131296635 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.signOut();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MainHealthProviderActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.drawerLayout = null;
    target.darwerList = null;
    target.toolbar = null;
    target.txtRole = null;
    target.username = null;
    target.drawerPic = null;

    view2131296672.setOnClickListener(null);
    view2131296672 = null;
    view2131296628.setOnClickListener(null);
    view2131296628 = null;
    view2131296635.setOnClickListener(null);
    view2131296635 = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.dashboard;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PatientMainFragment_ViewBinding implements Unbinder {
  private PatientMainFragment target;

  @UiThread
  public PatientMainFragment_ViewBinding(PatientMainFragment target, View source) {
    this.target = target;

    target.mainList = Utils.findRequiredViewAsType(source, R.id.main_list, "field 'mainList'", RecyclerView.class);
    target.emptyView = Utils.findRequiredViewAsType(source, R.id.empty_view, "field 'emptyView'", TextView.class);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.listviewSpace = res.getDimensionPixelSize(R.dimen.item_space_small);
  }

  @Override
  @CallSuper
  public void unbind() {
    PatientMainFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mainList = null;
    target.emptyView = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.mdconsults.widget;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ScheduleItem1_ViewBinding implements Unbinder {
  private ScheduleItem1 target;

  private View view2131296738;

  private View view2131296414;

  private View view2131296554;

  private View view2131296284;

  @UiThread
  public ScheduleItem1_ViewBinding(ScheduleItem1 target) {
    this(target, target);
  }

  @UiThread
  public ScheduleItem1_ViewBinding(final ScheduleItem1 target, View source) {
    this.target = target;

    View view;
    target.toTxt = Utils.findRequiredViewAsType(source, R.id.to_txt, "field 'toTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.to_time, "field 'toTime' and method 'onViewClicked'");
    target.toTime = Utils.castView(view, R.id.to_time, "field 'toTime'", LinearLayout.class);
    view2131296738 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.fromTxt = Utils.findRequiredViewAsType(source, R.id.from_txt, "field 'fromTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.from_time, "field 'fromTime' and method 'onViewClicked'");
    target.fromTime = Utils.castView(view, R.id.from_time, "field 'fromTime'", LinearLayout.class);
    view2131296414 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.toTxtAmPm = Utils.findRequiredViewAsType(source, R.id.to_txt_am_pm, "field 'toTxtAmPm'", TextView.class);
    target.fromTxtAmPm = Utils.findRequiredViewAsType(source, R.id.from_txt_am_pm, "field 'fromTxtAmPm'", TextView.class);
    view = Utils.findRequiredView(source, R.id.open_con_check, "field 'openConCheck' and method 'onChecked'");
    target.openConCheck = Utils.castView(view, R.id.open_con_check, "field 'openConCheck'", CheckBox.class);
    view2131296554 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onChecked(p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.add, "field 'add' and method 'onViewClicked'");
    target.add = Utils.castView(view, R.id.add, "field 'add'", ImageButton.class);
    view2131296284 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ScheduleItem1 target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toTxt = null;
    target.toTime = null;
    target.fromTxt = null;
    target.fromTime = null;
    target.toTxtAmPm = null;
    target.fromTxtAmPm = null;
    target.openConCheck = null;
    target.add = null;

    view2131296738.setOnClickListener(null);
    view2131296738 = null;
    view2131296414.setOnClickListener(null);
    view2131296414 = null;
    ((CompoundButton) view2131296554).setOnCheckedChangeListener(null);
    view2131296554 = null;
    view2131296284.setOnClickListener(null);
    view2131296284 = null;
  }
}

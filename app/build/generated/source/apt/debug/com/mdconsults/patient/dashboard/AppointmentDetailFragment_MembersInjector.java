package com.mdconsults.patient.dashboard;

import com.mdconsults.data.MDCRepository;
import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppointmentDetailFragment_MembersInjector
    implements MembersInjector<AppointmentDetailFragment> {
  private final Provider<MDCRepository> mdcRepositoryProvider;

  private final Provider<ApiFactory> apiFactoryProvider;

  public AppointmentDetailFragment_MembersInjector(
      Provider<MDCRepository> mdcRepositoryProvider, Provider<ApiFactory> apiFactoryProvider) {
    assert mdcRepositoryProvider != null;
    this.mdcRepositoryProvider = mdcRepositoryProvider;
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<AppointmentDetailFragment> create(
      Provider<MDCRepository> mdcRepositoryProvider, Provider<ApiFactory> apiFactoryProvider) {
    return new AppointmentDetailFragment_MembersInjector(mdcRepositoryProvider, apiFactoryProvider);
  }

  @Override
  public void injectMembers(AppointmentDetailFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.mdcRepository = mdcRepositoryProvider.get();
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectMdcRepository(
      AppointmentDetailFragment instance, Provider<MDCRepository> mdcRepositoryProvider) {
    instance.mdcRepository = mdcRepositoryProvider.get();
  }

  public static void injectApiFactory(
      AppointmentDetailFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

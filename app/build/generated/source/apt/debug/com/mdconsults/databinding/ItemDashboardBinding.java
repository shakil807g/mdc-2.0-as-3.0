package com.mdconsults.databinding;
import com.mdconsults.R;
import com.mdconsults.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
@javax.annotation.Generated("Android Data Binding")
public class ItemDashboardBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.item_main, 1);
        sViewsWithIds.put(R.id.txt_type, 2);
        sViewsWithIds.put(R.id.profile_image, 3);
        sViewsWithIds.put(R.id.textView3, 4);
        sViewsWithIds.put(R.id.textView8, 5);
        sViewsWithIds.put(R.id.doc_name, 6);
        sViewsWithIds.put(R.id.date, 7);
        sViewsWithIds.put(R.id.time, 8);
        sViewsWithIds.put(R.id.textView12, 9);
        sViewsWithIds.put(R.id.btn_view_case, 10);
        sViewsWithIds.put(R.id.video_icon, 11);
    }
    // views
    @NonNull
    public final android.widget.TextView btnViewCase;
    @NonNull
    public final android.widget.TextView date;
    @NonNull
    public final android.widget.TextView docName;
    @NonNull
    public final android.support.constraint.ConstraintLayout itemMain;
    @NonNull
    private final android.support.v7.widget.CardView mboundView0;
    @NonNull
    public final de.hdodenhof.circleimageview.CircleImageView profileImage;
    @NonNull
    public final android.widget.TextView textView12;
    @NonNull
    public final android.widget.TextView textView3;
    @NonNull
    public final android.widget.TextView textView8;
    @NonNull
    public final android.widget.TextView time;
    @NonNull
    public final android.widget.TextView txtType;
    @NonNull
    public final android.widget.ImageView videoIcon;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemDashboardBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds);
        this.btnViewCase = (android.widget.TextView) bindings[10];
        this.date = (android.widget.TextView) bindings[7];
        this.docName = (android.widget.TextView) bindings[6];
        this.itemMain = (android.support.constraint.ConstraintLayout) bindings[1];
        this.mboundView0 = (android.support.v7.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.profileImage = (de.hdodenhof.circleimageview.CircleImageView) bindings[3];
        this.textView12 = (android.widget.TextView) bindings[9];
        this.textView3 = (android.widget.TextView) bindings[4];
        this.textView8 = (android.widget.TextView) bindings[5];
        this.time = (android.widget.TextView) bindings[8];
        this.txtType = (android.widget.TextView) bindings[2];
        this.videoIcon = (android.widget.ImageView) bindings[11];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ItemDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ItemDashboardBinding>inflate(inflater, com.mdconsults.R.layout.item_dashboard, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ItemDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.mdconsults.R.layout.item_dashboard, null, false), bindingComponent);
    }
    @NonNull
    public static ItemDashboardBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemDashboardBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/item_dashboard_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ItemDashboardBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}
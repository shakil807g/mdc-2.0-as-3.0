package com.mdconsults.healthprovider;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class HealthProviderViewModel_MembersInjector
    implements MembersInjector<HealthProviderViewModel> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public HealthProviderViewModel_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<HealthProviderViewModel> create(
      Provider<ApiFactory> apiFactoryProvider) {
    return new HealthProviderViewModel_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(HealthProviderViewModel instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      HealthProviderViewModel instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

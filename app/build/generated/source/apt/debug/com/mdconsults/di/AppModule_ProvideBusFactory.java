package com.mdconsults.di;

import com.mdconsults.util.RxBus;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideBusFactory implements Factory<RxBus> {
  private final AppModule module;

  public AppModule_ProvideBusFactory(AppModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public RxBus get() {
    return Preconditions.checkNotNull(
        module.provideBus(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<RxBus> create(AppModule module) {
    return new AppModule_ProvideBusFactory(module);
  }

  /** Proxies {@link AppModule#provideBus()}. */
  public static RxBus proxyProvideBus(AppModule instance) {
    return instance.provideBus();
  }
}

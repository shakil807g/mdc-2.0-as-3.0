package com.mdconsults.data.local.Doa;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import com.mdconsults.data.model.MainAppointment;
import io.reactivex.Maybe;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.concurrent.Callable;

public class MainAppointmentDoa_Impl implements MainAppointmentDoa {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfMainAppointment;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public MainAppointmentDoa_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMainAppointment = new EntityInsertionAdapter<MainAppointment>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `MainAppointment`(`date`,`id`,`record_id`,`name`,`start_time`,`end_time`,`start_date`,`image`,`room_url`,`patient_id`) VALUES (?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MainAppointment value) {
        if (value.date == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.date);
        }
        if (value.id == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.id);
        }
        if (value.record_id == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.record_id);
        }
        if (value.name == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.name);
        }
        if (value.start_time == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.start_time);
        }
        if (value.end_time == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.end_time);
        }
        if (value.start_date == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.start_date);
        }
        if (value.image == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.image);
        }
        if (value.room_url == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.room_url);
        }
        if (value.patient_id == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.patient_id);
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM MainAppointment WHERE patient_id = ?";
        return _query;
      }
    };
  }

  @Override
  public void insertAll(List<MainAppointment> mainAppointments) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMainAppointment.insert(mainAppointments);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(String patient_id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (patient_id == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, patient_id);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public Maybe<List<MainAppointment>> getMainAppointment(String patient_id) {
    final String _sql = "SELECT * FROM MainAppointment WHERE patient_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (patient_id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, patient_id);
    }
    return Maybe.fromCallable(new Callable<List<MainAppointment>>() {
      public List<MainAppointment> call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfRecordId = _cursor.getColumnIndexOrThrow("record_id");
          final int _cursorIndexOfName = _cursor.getColumnIndexOrThrow("name");
          final int _cursorIndexOfStartTime = _cursor.getColumnIndexOrThrow("start_time");
          final int _cursorIndexOfEndTime = _cursor.getColumnIndexOrThrow("end_time");
          final int _cursorIndexOfStartDate = _cursor.getColumnIndexOrThrow("start_date");
          final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
          final int _cursorIndexOfRoomUrl = _cursor.getColumnIndexOrThrow("room_url");
          final int _cursorIndexOfPatientId = _cursor.getColumnIndexOrThrow("patient_id");
          final java.util.List<com.mdconsults.data.model.MainAppointment> _result = new java.util.ArrayList<com.mdconsults.data.model.MainAppointment>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final com.mdconsults.data.model.MainAppointment _item;
            _item = new com.mdconsults.data.model.MainAppointment();
            _item.date = _cursor.getString(_cursorIndexOfDate);
            _item.id = _cursor.getString(_cursorIndexOfId);
            _item.record_id = _cursor.getString(_cursorIndexOfRecordId);
            _item.name = _cursor.getString(_cursorIndexOfName);
            _item.start_time = _cursor.getString(_cursorIndexOfStartTime);
            _item.end_time = _cursor.getString(_cursorIndexOfEndTime);
            _item.start_date = _cursor.getString(_cursorIndexOfStartDate);
            _item.image = _cursor.getString(_cursorIndexOfImage);
            _item.room_url = _cursor.getString(_cursorIndexOfRoomUrl);
            _item.patient_id = _cursor.getString(_cursorIndexOfPatientId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    });
  }
}

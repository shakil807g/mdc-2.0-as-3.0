// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.healthinfo;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HealthInfoFragment_ViewBinding implements Unbinder {
  private HealthInfoFragment target;

  private View view2131296401;

  private View view2131296802;

  private View view2131296629;

  private View view2131296426;

  private View view2131296423;

  private View view2131296362;

  private View view2131296300;

  private View view2131296328;

  private View view2131296511;

  @UiThread
  public HealthInfoFragment_ViewBinding(final HealthInfoFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.first_aid, "method 'onViewClicked'");
    view2131296401 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.women_health, "method 'onViewClicked'");
    view2131296802 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sexual, "method 'onViewClicked'");
    view2131296629 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.high_blood, "method 'onViewClicked'");
    view2131296426 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.health, "method 'onViewClicked'");
    view2131296423 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.diabities, "method 'onViewClicked'");
    view2131296362 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.bal_diet, "method 'onViewClicked'");
    view2131296300 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.child_support, "method 'onViewClicked'");
    view2131296328 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mental, "method 'onViewClicked'");
    view2131296511 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view2131296401.setOnClickListener(null);
    view2131296401 = null;
    view2131296802.setOnClickListener(null);
    view2131296802 = null;
    view2131296629.setOnClickListener(null);
    view2131296629 = null;
    view2131296426.setOnClickListener(null);
    view2131296426 = null;
    view2131296423.setOnClickListener(null);
    view2131296423 = null;
    view2131296362.setOnClickListener(null);
    view2131296362 = null;
    view2131296300.setOnClickListener(null);
    view2131296300 = null;
    view2131296328.setOnClickListener(null);
    view2131296328 = null;
    view2131296511.setOnClickListener(null);
    view2131296511 = null;
  }
}

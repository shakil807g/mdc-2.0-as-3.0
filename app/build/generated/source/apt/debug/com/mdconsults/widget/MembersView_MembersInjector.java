package com.mdconsults.widget;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MembersView_MembersInjector implements MembersInjector<MembersView> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public MembersView_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<MembersView> create(Provider<ApiFactory> apiFactoryProvider) {
    return new MembersView_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(MembersView instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      MembersView instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.cases;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyCasesFragment_ViewBinding implements Unbinder {
  private MyCasesFragment target;

  @UiThread
  public MyCasesFragment_ViewBinding(MyCasesFragment target, View source) {
    this.target = target;

    target.mainList = Utils.findRequiredViewAsType(source, R.id.main_list, "field 'mainList'", RecyclerView.class);
    target.searchView = Utils.findRequiredViewAsType(source, R.id.search_view, "field 'searchView'", SearchView.class);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.listviewSpace = res.getDimensionPixelSize(R.dimen.item_space_small);
  }

  @Override
  @CallSuper
  public void unbind() {
    MyCasesFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mainList = null;
    target.searchView = null;
  }
}

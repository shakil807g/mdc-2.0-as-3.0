// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.schedule;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ScheduleFragment_ViewBinding implements Unbinder {
  private ScheduleFragment target;

  private View view2131296658;

  private View view2131296387;

  private View view2131296665;

  private View view2131296644;

  private View view2131296517;

  private View view2131296750;

  private View view2131296798;

  private View view2131296729;

  private View view2131296411;

  private View view2131296597;

  private View view2131296670;

  private View view2131296518;

  private View view2131296751;

  private View view2131296799;

  private View view2131296730;

  private View view2131296412;

  private View view2131296596;

  private View view2131296669;

  @UiThread
  public ScheduleFragment_ViewBinding(final ScheduleFragment target, View source) {
    this.target = target;

    View view;
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.main, "field 'scrollView'", ScrollView.class);
    view = Utils.findRequiredView(source, R.id.start_date, "field 'startDate' and method 'dateClick'");
    target.startDate = Utils.castView(view, R.id.start_date, "field 'startDate'", Button.class);
    view2131296658 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.dateClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.end_date, "field 'endDate' and method 'dateClick'");
    target.endDate = Utils.castView(view, R.id.end_date, "field 'endDate'", Button.class);
    view2131296387 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.dateClick(p0);
      }
    });
    target.allDaysTab = Utils.findRequiredViewAsType(source, R.id.all_days_tab, "field 'allDaysTab'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'submitClick'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", TextView.class);
    view2131296665 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submitClick();
      }
    });
    target.txtPrice = Utils.findRequiredViewAsType(source, R.id.txt_price, "field 'txtPrice'", EditText.class);
    target.mondayTab = Utils.findRequiredViewAsType(source, R.id.monday_tab, "field 'mondayTab'", LinearLayout.class);
    target.tuesdayTab = Utils.findRequiredViewAsType(source, R.id.tuesday_tab, "field 'tuesdayTab'", LinearLayout.class);
    target.wednesdayTab = Utils.findRequiredViewAsType(source, R.id.wednesday_tab, "field 'wednesdayTab'", LinearLayout.class);
    target.thrusdayTab = Utils.findRequiredViewAsType(source, R.id.thrusday_tab, "field 'thrusdayTab'", LinearLayout.class);
    target.fridayTab = Utils.findRequiredViewAsType(source, R.id.friday_tab, "field 'fridayTab'", LinearLayout.class);
    target.satTab = Utils.findRequiredViewAsType(source, R.id.sat_tab, "field 'satTab'", LinearLayout.class);
    target.sunTab = Utils.findRequiredViewAsType(source, R.id.sun_tab, "field 'sunTab'", LinearLayout.class);
    target.previous_data = Utils.findRequiredViewAsType(source, R.id.previous_data, "field 'previous_data'", TextView.class);
    view = Utils.findRequiredView(source, R.id.spin_duration, "field 'spinDuration' and method 'onItemSelected'");
    target.spinDuration = Utils.castView(view, R.id.spin_duration, "field 'spinDuration'", Spinner.class);
    view2131296644 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.onItemSelected(p2);
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    target.preDateCard = Utils.findRequiredViewAsType(source, R.id.pre_date_card, "field 'preDateCard'", CardView.class);
    view = Utils.findRequiredView(source, R.id.mon_not_avail, "method 'onViewClicked'");
    view2131296517 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onViewClicked(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.tue_not_avail, "method 'onViewClicked'");
    view2131296750 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onViewClicked(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.wed_not_avail, "method 'onViewClicked'");
    view2131296798 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onViewClicked(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.thr_not_avail, "method 'onViewClicked'");
    view2131296729 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onViewClicked(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.fri_not_avail, "method 'onViewClicked'");
    view2131296411 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onViewClicked(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.sat_not_avail, "method 'onViewClicked'");
    view2131296597 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onViewClicked(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.sun_not_avail, "method 'onViewClicked'");
    view2131296670 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onViewClicked(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.monday, "method 'onViewClicked'");
    view2131296518 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tuesday, "method 'onViewClicked'");
    view2131296751 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.wednesday, "method 'onViewClicked'");
    view2131296799 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.thrusday, "method 'onViewClicked'");
    view2131296730 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.friday, "method 'onViewClicked'");
    view2131296412 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sat, "method 'onViewClicked'");
    view2131296596 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sun, "method 'onViewClicked'");
    view2131296669 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ScheduleFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.scrollView = null;
    target.startDate = null;
    target.endDate = null;
    target.allDaysTab = null;
    target.submit = null;
    target.txtPrice = null;
    target.mondayTab = null;
    target.tuesdayTab = null;
    target.wednesdayTab = null;
    target.thrusdayTab = null;
    target.fridayTab = null;
    target.satTab = null;
    target.sunTab = null;
    target.previous_data = null;
    target.spinDuration = null;
    target.preDateCard = null;

    view2131296658.setOnClickListener(null);
    view2131296658 = null;
    view2131296387.setOnClickListener(null);
    view2131296387 = null;
    view2131296665.setOnClickListener(null);
    view2131296665 = null;
    ((AdapterView<?>) view2131296644).setOnItemSelectedListener(null);
    view2131296644 = null;
    ((CompoundButton) view2131296517).setOnCheckedChangeListener(null);
    view2131296517 = null;
    ((CompoundButton) view2131296750).setOnCheckedChangeListener(null);
    view2131296750 = null;
    ((CompoundButton) view2131296798).setOnCheckedChangeListener(null);
    view2131296798 = null;
    ((CompoundButton) view2131296729).setOnCheckedChangeListener(null);
    view2131296729 = null;
    ((CompoundButton) view2131296411).setOnCheckedChangeListener(null);
    view2131296411 = null;
    ((CompoundButton) view2131296597).setOnCheckedChangeListener(null);
    view2131296597 = null;
    ((CompoundButton) view2131296670).setOnCheckedChangeListener(null);
    view2131296670 = null;
    view2131296518.setOnClickListener(null);
    view2131296518 = null;
    view2131296751.setOnClickListener(null);
    view2131296751 = null;
    view2131296799.setOnClickListener(null);
    view2131296799 = null;
    view2131296730.setOnClickListener(null);
    view2131296730 = null;
    view2131296412.setOnClickListener(null);
    view2131296412 = null;
    view2131296596.setOnClickListener(null);
    view2131296596 = null;
    view2131296669.setOnClickListener(null);
    view2131296669 = null;
  }
}

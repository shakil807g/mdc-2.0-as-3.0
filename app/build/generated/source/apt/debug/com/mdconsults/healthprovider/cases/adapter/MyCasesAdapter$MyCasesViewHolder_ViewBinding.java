// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.cases.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyCasesAdapter$MyCasesViewHolder_ViewBinding implements Unbinder {
  private MyCasesAdapter.MyCasesViewHolder target;

  @UiThread
  public MyCasesAdapter$MyCasesViewHolder_ViewBinding(MyCasesAdapter.MyCasesViewHolder target, View source) {
    this.target = target;

    target.txtCaseId = Utils.findRequiredViewAsType(source, R.id.txt_case_id, "field 'txtCaseId'", TextView.class);
    target.txtName = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'txtName'", TextView.class);
    target.txtLocation = Utils.findRequiredViewAsType(source, R.id.txt_location, "field 'txtLocation'", TextView.class);
    target.txtTime = Utils.findRequiredViewAsType(source, R.id.txt_time, "field 'txtTime'", TextView.class);
    target.reffer_name = Utils.findRequiredViewAsType(source, R.id.reffer_name, "field 'reffer_name'", TextView.class);
    target.textView11 = Utils.findRequiredViewAsType(source, R.id.textView11, "field 'textView11'", TextView.class);
    target.group_name = Utils.findRequiredViewAsType(source, R.id.group_name, "field 'group_name'", TextView.class);
    target.label_date_of_review = Utils.findRequiredViewAsType(source, R.id.label_date_of_review, "field 'label_date_of_review'", TextView.class);
    target.txt_date_of_review = Utils.findRequiredViewAsType(source, R.id.txt_date_of_review, "field 'txt_date_of_review'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MyCasesAdapter.MyCasesViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtCaseId = null;
    target.txtName = null;
    target.txtLocation = null;
    target.txtTime = null;
    target.reffer_name = null;
    target.textView11 = null;
    target.group_name = null;
    target.label_date_of_review = null;
    target.txt_date_of_review = null;
  }
}

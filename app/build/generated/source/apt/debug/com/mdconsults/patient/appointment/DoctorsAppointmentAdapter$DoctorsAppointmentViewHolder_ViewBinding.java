// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.appointment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DoctorsAppointmentAdapter$DoctorsAppointmentViewHolder_ViewBinding implements Unbinder {
  private DoctorsAppointmentAdapter.DoctorsAppointmentViewHolder target;

  @UiThread
  public DoctorsAppointmentAdapter$DoctorsAppointmentViewHolder_ViewBinding(DoctorsAppointmentAdapter.DoctorsAppointmentViewHolder target, View source) {
    this.target = target;

    target.profileImage = Utils.findRequiredViewAsType(source, R.id.profile_image, "field 'profileImage'", CircleImageView.class);
    target.name = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'name'", TextView.class);
    target.type = Utils.findRequiredViewAsType(source, R.id.txt_type, "field 'type'", TextView.class);
    target.view_slots = Utils.findRequiredViewAsType(source, R.id.view_slots, "field 'view_slots'", TextView.class);
    target.txt_location = Utils.findRequiredViewAsType(source, R.id.txt_location, "field 'txt_location'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DoctorsAppointmentAdapter.DoctorsAppointmentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.profileImage = null;
    target.name = null;
    target.type = null;
    target.view_slots = null;
    target.txt_location = null;
  }
}

package com.mdconsults.healthprovider.group;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GroupDetailFragment_MembersInjector
    implements MembersInjector<GroupDetailFragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public GroupDetailFragment_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<GroupDetailFragment> create(
      Provider<ApiFactory> apiFactoryProvider) {
    return new GroupDetailFragment_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(GroupDetailFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      GroupDetailFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

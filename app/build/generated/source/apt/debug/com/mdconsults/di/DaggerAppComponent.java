package com.mdconsults.di;

import android.app.Application;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.mdconsults.authenticate.LoginActivity;
import com.mdconsults.authenticate.LoginActivity_MembersInjector;
import com.mdconsults.authenticate.SignUpActivity;
import com.mdconsults.authenticate.SignUpActivity_MembersInjector;
import com.mdconsults.data.MDCRepository;
import com.mdconsults.data.MDCRepository_Factory;
import com.mdconsults.data.local.Doa.MainAppointmentDetailDoa;
import com.mdconsults.data.local.Doa.MainAppointmentDoa;
import com.mdconsults.data.local.MDCDatabase;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.healthprovider.HealthProviderViewModel;
import com.mdconsults.healthprovider.HealthProviderViewModel_MembersInjector;
import com.mdconsults.healthprovider.MainHealthProviderActivity;
import com.mdconsults.healthprovider.appointment.HealthProviderAppointmentFragment;
import com.mdconsults.healthprovider.appointment.HealthProviderAppointmentFragment_MembersInjector;
import com.mdconsults.healthprovider.cases.CaseDetailFragment;
import com.mdconsults.healthprovider.cases.CaseDetailFragment_MembersInjector;
import com.mdconsults.healthprovider.cases.CreateCaseFragment;
import com.mdconsults.healthprovider.cases.CreateCaseFragment_MembersInjector;
import com.mdconsults.healthprovider.cases.MyCasesFragment;
import com.mdconsults.healthprovider.cases.MyCasesFragment_MembersInjector;
import com.mdconsults.healthprovider.group.GroupDetailFragment;
import com.mdconsults.healthprovider.group.GroupDetailFragment_MembersInjector;
import com.mdconsults.healthprovider.group.GroupFragment;
import com.mdconsults.healthprovider.group.GroupFragment_MembersInjector;
import com.mdconsults.healthprovider.group.NewGroupDailog;
import com.mdconsults.healthprovider.group.NewGroupDailog_MembersInjector;
import com.mdconsults.healthprovider.schedule.ScheduleFragment;
import com.mdconsults.healthprovider.schedule.ScheduleFragment_MembersInjector;
import com.mdconsults.healthprovider.settings.AddLocationDialog;
import com.mdconsults.healthprovider.settings.AddLocationDialog_MembersInjector;
import com.mdconsults.patient.PatientViewModel;
import com.mdconsults.patient.PatientViewModel_MembersInjector;
import com.mdconsults.patient.appointment.BookAppointmentService;
import com.mdconsults.patient.appointment.BookAppointmentService_MembersInjector;
import com.mdconsults.patient.appointment.PatientAppointmentFragment;
import com.mdconsults.patient.appointment.Step3Fragment;
import com.mdconsults.patient.appointment.Step3Fragment_MembersInjector;
import com.mdconsults.patient.appointment.Step4Fragment;
import com.mdconsults.patient.appointment.Step4Fragment_MembersInjector;
import com.mdconsults.patient.appointment.StepFragment;
import com.mdconsults.patient.caregiver.AddCareGiverDailog;
import com.mdconsults.patient.caregiver.AddCareGiverDailog_MembersInjector;
import com.mdconsults.patient.caregiver.CareGiverFragment;
import com.mdconsults.patient.caregiver.CareGiverFragment_MembersInjector;
import com.mdconsults.patient.dashboard.AppointmentDetailFragment;
import com.mdconsults.patient.dashboard.AppointmentDetailFragment_MembersInjector;
import com.mdconsults.patient.dashboard.PatientMainFragment;
import com.mdconsults.patient.dashboard.PatientMainFragment_MembersInjector;
import com.mdconsults.setting.SettingActivity;
import com.mdconsults.setting.SettingActivity_MembersInjector;
import com.mdconsults.util.RxBus;
import com.mdconsults.widget.MembersView;
import com.mdconsults.widget.MembersView_MembersInjector;
import dagger.MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.MembersInjectors;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerAppComponent implements AppComponent {
  private Provider<Gson> provideGsonProvider;

  private Provider<Application> providesApplicationProvider;

  private Provider<Cache> provideOkHttpCacheProvider;

  private Provider<OkHttpClient> provideOkHttpClientProvider;

  private Provider<Retrofit> provideRetrofitProvider;

  private Provider<ApiFactory> provideMDCApiProvider;

  private MembersInjector<LoginActivity> loginActivityMembersInjector;

  private MembersInjector<SignUpActivity> signUpActivityMembersInjector;

  private Provider<RxBus> provideBusProvider;

  private MembersInjector<Step4Fragment> step4FragmentMembersInjector;

  private MembersInjector<Step3Fragment> step3FragmentMembersInjector;

  private Provider<MDCDatabase> provideDbProvider;

  private Provider<MainAppointmentDoa> provideMainAppointmentDoaProvider;

  private Provider<MainAppointmentDetailDoa> provideMainAppointmentDetailProvider;

  private Provider<MDCRepository> mDCRepositoryProvider;

  private MembersInjector<PatientMainFragment> patientMainFragmentMembersInjector;

  private MembersInjector<ScheduleFragment> scheduleFragmentMembersInjector;

  private MembersInjector<AppointmentDetailFragment> appointmentDetailFragmentMembersInjector;

  private MembersInjector<BookAppointmentService> bookAppointmentServiceMembersInjector;

  private MembersInjector<CreateCaseFragment> createCaseFragmentMembersInjector;

  private MembersInjector<MyCasesFragment> myCasesFragmentMembersInjector;

  private MembersInjector<CaseDetailFragment> caseDetailFragmentMembersInjector;

  private MembersInjector<NewGroupDailog> newGroupDailogMembersInjector;

  private MembersInjector<MembersView> membersViewMembersInjector;

  private MembersInjector<GroupFragment> groupFragmentMembersInjector;

  private MembersInjector<AddLocationDialog> addLocationDialogMembersInjector;

  private Provider<SharedPreferences> providesSharedPreferencesProvider;

  private MembersInjector<AddCareGiverDailog> addCareGiverDailogMembersInjector;

  private MembersInjector<CareGiverFragment> careGiverFragmentMembersInjector;

  private MembersInjector<HealthProviderAppointmentFragment>
      healthProviderAppointmentFragmentMembersInjector;

  private MembersInjector<SettingActivity> settingActivityMembersInjector;

  private MembersInjector<GroupDetailFragment> groupDetailFragmentMembersInjector;

  private MembersInjector<HealthProviderViewModel> healthProviderViewModelMembersInjector;

  private MembersInjector<PatientViewModel> patientViewModelMembersInjector;

  private DaggerAppComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.provideGsonProvider =
        DoubleCheck.provider(AppModule_ProvideGsonFactory.create(builder.appModule));

    this.providesApplicationProvider =
        DoubleCheck.provider(AppModule_ProvidesApplicationFactory.create(builder.appModule));

    this.provideOkHttpCacheProvider =
        DoubleCheck.provider(
            AppModule_ProvideOkHttpCacheFactory.create(
                builder.appModule, providesApplicationProvider));

    this.provideOkHttpClientProvider =
        DoubleCheck.provider(
            AppModule_ProvideOkHttpClientFactory.create(
                builder.appModule, provideOkHttpCacheProvider));

    this.provideRetrofitProvider =
        DoubleCheck.provider(
            AppModule_ProvideRetrofitFactory.create(
                builder.appModule, provideGsonProvider, provideOkHttpClientProvider));

    this.provideMDCApiProvider =
        DoubleCheck.provider(
            AppModule_ProvideMDCApiFactory.create(builder.appModule, provideRetrofitProvider));

    this.loginActivityMembersInjector = LoginActivity_MembersInjector.create(provideMDCApiProvider);

    this.signUpActivityMembersInjector =
        SignUpActivity_MembersInjector.create(provideMDCApiProvider);

    this.provideBusProvider =
        DoubleCheck.provider(AppModule_ProvideBusFactory.create(builder.appModule));

    this.step4FragmentMembersInjector =
        Step4Fragment_MembersInjector.create(provideMDCApiProvider, provideBusProvider);

    this.step3FragmentMembersInjector = Step3Fragment_MembersInjector.create(provideMDCApiProvider);

    this.provideDbProvider =
        DoubleCheck.provider(
            AppModule_ProvideDbFactory.create(builder.appModule, providesApplicationProvider));

    this.provideMainAppointmentDoaProvider =
        DoubleCheck.provider(
            AppModule_ProvideMainAppointmentDoaFactory.create(
                builder.appModule, provideDbProvider));

    this.provideMainAppointmentDetailProvider =
        DoubleCheck.provider(
            AppModule_ProvideMainAppointmentDetailFactory.create(
                builder.appModule, provideDbProvider));

    this.mDCRepositoryProvider =
        DoubleCheck.provider(
            MDCRepository_Factory.create(
                provideMDCApiProvider,
                provideDbProvider,
                provideMainAppointmentDoaProvider,
                provideMainAppointmentDetailProvider));

    this.patientMainFragmentMembersInjector =
        PatientMainFragment_MembersInjector.create(
            provideMDCApiProvider, provideBusProvider, mDCRepositoryProvider);

    this.scheduleFragmentMembersInjector =
        ScheduleFragment_MembersInjector.create(provideMDCApiProvider);

    this.appointmentDetailFragmentMembersInjector =
        AppointmentDetailFragment_MembersInjector.create(
            mDCRepositoryProvider, provideMDCApiProvider);

    this.bookAppointmentServiceMembersInjector =
        BookAppointmentService_MembersInjector.create(provideBusProvider, provideMDCApiProvider);

    this.createCaseFragmentMembersInjector =
        CreateCaseFragment_MembersInjector.create(
            provideMDCApiProvider, provideOkHttpClientProvider);

    this.myCasesFragmentMembersInjector =
        MyCasesFragment_MembersInjector.create(provideBusProvider, provideMDCApiProvider);

    this.caseDetailFragmentMembersInjector =
        CaseDetailFragment_MembersInjector.create(provideMDCApiProvider);

    this.newGroupDailogMembersInjector =
        NewGroupDailog_MembersInjector.create(provideMDCApiProvider);

    this.membersViewMembersInjector = MembersView_MembersInjector.create(provideMDCApiProvider);

    this.groupFragmentMembersInjector = GroupFragment_MembersInjector.create(provideMDCApiProvider);

    this.addLocationDialogMembersInjector =
        AddLocationDialog_MembersInjector.create(provideMDCApiProvider);

    this.providesSharedPreferencesProvider =
        DoubleCheck.provider(
            AppModule_ProvidesSharedPreferencesFactory.create(
                builder.appModule, providesApplicationProvider));

    this.addCareGiverDailogMembersInjector =
        AddCareGiverDailog_MembersInjector.create(provideMDCApiProvider);

    this.careGiverFragmentMembersInjector =
        CareGiverFragment_MembersInjector.create(provideMDCApiProvider);

    this.healthProviderAppointmentFragmentMembersInjector =
        HealthProviderAppointmentFragment_MembersInjector.create(provideMDCApiProvider);

    this.settingActivityMembersInjector =
        SettingActivity_MembersInjector.create(provideMDCApiProvider);

    this.groupDetailFragmentMembersInjector =
        GroupDetailFragment_MembersInjector.create(provideMDCApiProvider);

    this.healthProviderViewModelMembersInjector =
        HealthProviderViewModel_MembersInjector.create(provideMDCApiProvider);

    this.patientViewModelMembersInjector =
        PatientViewModel_MembersInjector.create(mDCRepositoryProvider);
  }

  @Override
  public void inject(MainHealthProviderActivity activity) {
    MembersInjectors.<MainHealthProviderActivity>noOp().injectMembers(activity);
  }

  @Override
  public void inject(LoginActivity activity) {
    loginActivityMembersInjector.injectMembers(activity);
  }

  @Override
  public void inject(SignUpActivity activity) {
    signUpActivityMembersInjector.injectMembers(activity);
  }

  @Override
  public void inject(StepFragment stepFragment) {
    MembersInjectors.<StepFragment>noOp().injectMembers(stepFragment);
  }

  @Override
  public void inject(Step4Fragment stepFragment) {
    step4FragmentMembersInjector.injectMembers(stepFragment);
  }

  @Override
  public void inject(Step3Fragment stepFragment) {
    step3FragmentMembersInjector.injectMembers(stepFragment);
  }

  @Override
  public void inject(PatientMainFragment patientMainFragment) {
    patientMainFragmentMembersInjector.injectMembers(patientMainFragment);
  }

  @Override
  public void inject(PatientAppointmentFragment patientAppointmentFragment) {
    MembersInjectors.<PatientAppointmentFragment>noOp().injectMembers(patientAppointmentFragment);
  }

  @Override
  public void inject(ScheduleFragment scheduleFragment) {
    scheduleFragmentMembersInjector.injectMembers(scheduleFragment);
  }

  @Override
  public void inject(AppointmentDetailFragment appointmentDetailFragment) {
    appointmentDetailFragmentMembersInjector.injectMembers(appointmentDetailFragment);
  }

  @Override
  public void inject(BookAppointmentService imageUploadService) {
    bookAppointmentServiceMembersInjector.injectMembers(imageUploadService);
  }

  @Override
  public void inject(CreateCaseFragment createCaseFragment) {
    createCaseFragmentMembersInjector.injectMembers(createCaseFragment);
  }

  @Override
  public void inject(MyCasesFragment myCasesFragment) {
    myCasesFragmentMembersInjector.injectMembers(myCasesFragment);
  }

  @Override
  public void inject(CaseDetailFragment caseDetailFragment) {
    caseDetailFragmentMembersInjector.injectMembers(caseDetailFragment);
  }

  @Override
  public void inject(NewGroupDailog newGroupDailog) {
    newGroupDailogMembersInjector.injectMembers(newGroupDailog);
  }

  @Override
  public void inject(MembersView membersView) {
    membersViewMembersInjector.injectMembers(membersView);
  }

  @Override
  public void inject(GroupFragment groupFragment) {
    groupFragmentMembersInjector.injectMembers(groupFragment);
  }

  @Override
  public void inject(AddLocationDialog addLocationDialog) {
    addLocationDialogMembersInjector.injectMembers(addLocationDialog);
  }

  @Override
  public Application provideApp() {
    return providesApplicationProvider.get();
  }

  @Override
  public Retrofit retrofit() {
    return provideRetrofitProvider.get();
  }

  @Override
  public Gson gson() {
    return provideGsonProvider.get();
  }

  @Override
  public OkHttpClient okHttpClient() {
    return provideOkHttpClientProvider.get();
  }

  @Override
  public SharedPreferences sharedPreferences() {
    return providesSharedPreferencesProvider.get();
  }

  @Override
  public ApiFactory mdcApi() {
    return provideMDCApiProvider.get();
  }

  @Override
  public void inject(AddCareGiverDailog addCareGiverDailog) {
    addCareGiverDailogMembersInjector.injectMembers(addCareGiverDailog);
  }

  @Override
  public void inject(CareGiverFragment careGiverFragment) {
    careGiverFragmentMembersInjector.injectMembers(careGiverFragment);
  }

  @Override
  public void inject(HealthProviderAppointmentFragment healthProviderAppointmentFragment) {
    healthProviderAppointmentFragmentMembersInjector.injectMembers(
        healthProviderAppointmentFragment);
  }

  @Override
  public void inject(SettingActivity settingActivity) {
    settingActivityMembersInjector.injectMembers(settingActivity);
  }

  @Override
  public void inject(GroupDetailFragment groupDetailFragment) {
    groupDetailFragmentMembersInjector.injectMembers(groupDetailFragment);
  }

  @Override
  public void inject(HealthProviderViewModel healthProviderViewModel) {
    healthProviderViewModelMembersInjector.injectMembers(healthProviderViewModel);
  }

  @Override
  public void inject(PatientViewModel patientViewModel) {
    patientViewModelMembersInjector.injectMembers(patientViewModel);
  }

  public static final class Builder {
    private AppModule appModule;

    private Builder() {}

    public AppComponent build() {
      if (appModule == null) {
        throw new IllegalStateException(AppModule.class.getCanonicalName() + " must be set");
      }
      return new DaggerAppComponent(this);
    }

    public Builder appModule(AppModule appModule) {
      this.appModule = Preconditions.checkNotNull(appModule);
      return this;
    }
  }
}

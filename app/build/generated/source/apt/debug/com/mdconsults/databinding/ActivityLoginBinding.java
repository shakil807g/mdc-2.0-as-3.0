package com.mdconsults.databinding;
import com.mdconsults.R;
import com.mdconsults.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
@javax.annotation.Generated("Android Data Binding")
public class ActivityLoginBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView2, 1);
        sViewsWithIds.put(R.id.button, 2);
        sViewsWithIds.put(R.id.checkBox, 3);
        sViewsWithIds.put(R.id.forgot_pass, 4);
        sViewsWithIds.put(R.id.username, 5);
        sViewsWithIds.put(R.id.password, 6);
        sViewsWithIds.put(R.id.radio_group, 7);
        sViewsWithIds.put(R.id.patient_radio, 8);
        sViewsWithIds.put(R.id.health_radio, 9);
        sViewsWithIds.put(R.id.signup, 10);
        sViewsWithIds.put(R.id.shabir, 11);
        sViewsWithIds.put(R.id.dr_andreafay, 12);
    }
    // views
    @NonNull
    public final android.widget.Button button;
    @NonNull
    public final android.widget.CheckBox checkBox;
    @NonNull
    public final android.widget.Button drAndreafay;
    @NonNull
    public final android.widget.TextView forgotPass;
    @NonNull
    public final android.widget.RadioButton healthRadio;
    @NonNull
    public final android.widget.ImageView imageView2;
    @NonNull
    private final android.support.constraint.ConstraintLayout mboundView0;
    @NonNull
    public final android.widget.EditText password;
    @NonNull
    public final android.widget.RadioButton patientRadio;
    @NonNull
    public final android.widget.RadioGroup radioGroup;
    @NonNull
    public final android.widget.Button shabir;
    @NonNull
    public final android.widget.Button signup;
    @NonNull
    public final android.widget.EditText username;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityLoginBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds);
        this.button = (android.widget.Button) bindings[2];
        this.checkBox = (android.widget.CheckBox) bindings[3];
        this.drAndreafay = (android.widget.Button) bindings[12];
        this.forgotPass = (android.widget.TextView) bindings[4];
        this.healthRadio = (android.widget.RadioButton) bindings[9];
        this.imageView2 = (android.widget.ImageView) bindings[1];
        this.mboundView0 = (android.support.constraint.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.password = (android.widget.EditText) bindings[6];
        this.patientRadio = (android.widget.RadioButton) bindings[8];
        this.radioGroup = (android.widget.RadioGroup) bindings[7];
        this.shabir = (android.widget.Button) bindings[11];
        this.signup = (android.widget.Button) bindings[10];
        this.username = (android.widget.EditText) bindings[5];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ActivityLoginBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityLoginBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ActivityLoginBinding>inflate(inflater, com.mdconsults.R.layout.activity_login, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ActivityLoginBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityLoginBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.mdconsults.R.layout.activity_login, null, false), bindingComponent);
    }
    @NonNull
    public static ActivityLoginBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityLoginBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/activity_login_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ActivityLoginBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}
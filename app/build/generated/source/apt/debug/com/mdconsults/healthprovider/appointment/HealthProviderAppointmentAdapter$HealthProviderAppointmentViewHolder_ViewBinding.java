// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.appointment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HealthProviderAppointmentAdapter$HealthProviderAppointmentViewHolder_ViewBinding implements Unbinder {
  private HealthProviderAppointmentAdapter.HealthProviderAppointmentViewHolder target;

  @UiThread
  public HealthProviderAppointmentAdapter$HealthProviderAppointmentViewHolder_ViewBinding(HealthProviderAppointmentAdapter.HealthProviderAppointmentViewHolder target, View source) {
    this.target = target;

    target.txtDate = Utils.findRequiredViewAsType(source, R.id.txt_date, "field 'txtDate'", TextView.class);
    target.txtTime = Utils.findRequiredViewAsType(source, R.id.txt_time, "field 'txtTime'", TextView.class);
    target.referrerName = Utils.findRequiredViewAsType(source, R.id.referrer_name, "field 'referrerName'", TextView.class);
    target.btnViewCase = Utils.findRequiredViewAsType(source, R.id.btn_view_case, "field 'btnViewCase'", TextView.class);
    target.videoIcon = Utils.findRequiredViewAsType(source, R.id.video_icon, "field 'videoIcon'", ImageView.class);
    target.reffer_txt = Utils.findRequiredViewAsType(source, R.id.reffer_txt, "field 'reffer_txt'", TextView.class);
    target.case_code = Utils.findRequiredViewAsType(source, R.id.case_code, "field 'case_code'", TextView.class);
    target.patient_name = Utils.findRequiredViewAsType(source, R.id.patient_name, "field 'patient_name'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HealthProviderAppointmentAdapter.HealthProviderAppointmentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtDate = null;
    target.txtTime = null;
    target.referrerName = null;
    target.btnViewCase = null;
    target.videoIcon = null;
    target.reffer_txt = null;
    target.case_code = null;
    target.patient_name = null;
  }
}

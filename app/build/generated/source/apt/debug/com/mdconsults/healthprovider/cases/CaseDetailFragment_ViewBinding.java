// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.cases;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import com.mdconsults.widget.FlowLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CaseDetailFragment_ViewBinding implements Unbinder {
  private CaseDetailFragment target;

  private View view2131296368;

  private View view2131296626;

  @UiThread
  public CaseDetailFragment_ViewBinding(final CaseDetailFragment target, View source) {
    this.target = target;

    View view;
    target.txtGender = Utils.findRequiredViewAsType(source, R.id.txt_gender, "field 'txtGender'", TextView.class);
    target.txtLocation = Utils.findRequiredViewAsType(source, R.id.txt_location, "field 'txtLocation'", TextView.class);
    target.txtAge = Utils.findRequiredViewAsType(source, R.id.txt_age, "field 'txtAge'", TextView.class);
    target.txtDatetime = Utils.findRequiredViewAsType(source, R.id.txt_datetime, "field 'txtDatetime'", TextView.class);
    target.txt_medical_complain = Utils.findRequiredViewAsType(source, R.id.txt_medical_complain, "field 'txt_medical_complain'", TextView.class);
    target.txtHistory = Utils.findRequiredViewAsType(source, R.id.txt_history, "field 'txtHistory'", TextView.class);
    target.imageContainer = Utils.findRequiredViewAsType(source, R.id.image_container, "field 'imageContainer'", FlowLayout.class);
    target.label_comment = Utils.findRequiredViewAsType(source, R.id.label_comment, "field 'label_comment'", TextView.class);
    view = Utils.findRequiredView(source, R.id.doctors_spin, "field 'doctors_spin' and method 'onItemSelected'");
    target.doctors_spin = Utils.castView(view, R.id.doctors_spin, "field 'doctors_spin'", Spinner.class);
    view2131296368 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.onItemSelected(p2);
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    target.txt_book_appointment = Utils.findRequiredViewAsType(source, R.id.txt_book_appointment, "field 'txt_book_appointment'", TextView.class);
    target.txt_case_code = Utils.findRequiredViewAsType(source, R.id.txt_case_code, "field 'txt_case_code'", TextView.class);
    target.txt_gender_field = Utils.findRequiredViewAsType(source, R.id.txt_gender_field, "field 'txt_gender_field'", TextView.class);
    view = Utils.findRequiredView(source, R.id.send_comment, "field 'send_comment' and method 'sendComment'");
    target.send_comment = Utils.castView(view, R.id.send_comment, "field 'send_comment'", Button.class);
    view2131296626 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.sendComment();
      }
    });
    target.add_comment = Utils.findRequiredViewAsType(source, R.id.add_comment, "field 'add_comment'", EditText.class);
    target.comment_container = Utils.findRequiredViewAsType(source, R.id.comment_container, "field 'comment_container'", LinearLayout.class);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.dp_150 = res.getDimensionPixelSize(R.dimen.dp_150);
    target.dp_10 = res.getDimensionPixelSize(R.dimen.dp_10);
  }

  @Override
  @CallSuper
  public void unbind() {
    CaseDetailFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtGender = null;
    target.txtLocation = null;
    target.txtAge = null;
    target.txtDatetime = null;
    target.txt_medical_complain = null;
    target.txtHistory = null;
    target.imageContainer = null;
    target.label_comment = null;
    target.doctors_spin = null;
    target.txt_book_appointment = null;
    target.txt_case_code = null;
    target.txt_gender_field = null;
    target.send_comment = null;
    target.add_comment = null;
    target.comment_container = null;

    ((AdapterView<?>) view2131296368).setOnItemSelectedListener(null);
    view2131296368 = null;
    view2131296626.setOnClickListener(null);
    view2131296626 = null;
  }
}

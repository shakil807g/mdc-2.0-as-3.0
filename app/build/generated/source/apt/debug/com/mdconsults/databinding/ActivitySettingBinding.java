package com.mdconsults.databinding;
import com.mdconsults.R;
import com.mdconsults.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
@javax.annotation.Generated("Android Data Binding")
public class ActivitySettingBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.parent_image, 10);
        sViewsWithIds.put(R.id.edit_profile_small, 11);
        sViewsWithIds.put(R.id.label_speciality, 12);
        sViewsWithIds.put(R.id.spin_gender, 13);
        sViewsWithIds.put(R.id.edit_pass, 14);
        sViewsWithIds.put(R.id.edit_repass, 15);
        sViewsWithIds.put(R.id.lable_degree, 16);
        sViewsWithIds.put(R.id.label_location, 17);
        sViewsWithIds.put(R.id.places_autocomplete, 18);
        sViewsWithIds.put(R.id.label_city, 19);
        sViewsWithIds.put(R.id.label_state, 20);
        sViewsWithIds.put(R.id.label_country, 21);
        sViewsWithIds.put(R.id.save_btn, 22);
    }
    // views
    @NonNull
    public final android.widget.EditText city;
    @NonNull
    public final android.widget.EditText country;
    @NonNull
    public final android.widget.EditText degree;
    @NonNull
    public final android.widget.EditText editEmail;
    @NonNull
    public final android.widget.EditText editName;
    @NonNull
    public final android.widget.EditText editPass;
    @NonNull
    public final android.widget.ImageView editProfileSmall;
    @NonNull
    public final android.widget.EditText editRepass;
    @NonNull
    public final android.widget.EditText editSpeciality;
    @NonNull
    public final android.widget.EditText editUsername;
    @NonNull
    public final com.pkmmte.view.CircularImageView image;
    @NonNull
    public final android.widget.TextView labelCity;
    @NonNull
    public final android.widget.TextView labelCountry;
    @NonNull
    public final android.widget.TextView labelLocation;
    @NonNull
    public final android.widget.TextView labelSpeciality;
    @NonNull
    public final android.widget.TextView labelState;
    @NonNull
    public final android.widget.TextView lableDegree;
    @NonNull
    private final android.widget.ScrollView mboundView0;
    @NonNull
    public final android.widget.FrameLayout parentImage;
    @NonNull
    public final com.seatgeek.placesautocomplete.PlacesAutocompleteTextView placesAutocomplete;
    @NonNull
    public final android.widget.Button saveBtn;
    @NonNull
    public final android.widget.Spinner spinGender;
    @NonNull
    public final android.widget.EditText state;
    // variables
    @Nullable
    private com.mdconsults.data.model.User mUser;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivitySettingBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 23, sIncludes, sViewsWithIds);
        this.city = (android.widget.EditText) bindings[7];
        this.city.setTag(null);
        this.country = (android.widget.EditText) bindings[9];
        this.country.setTag(null);
        this.degree = (android.widget.EditText) bindings[6];
        this.degree.setTag(null);
        this.editEmail = (android.widget.EditText) bindings[5];
        this.editEmail.setTag(null);
        this.editName = (android.widget.EditText) bindings[2];
        this.editName.setTag(null);
        this.editPass = (android.widget.EditText) bindings[14];
        this.editProfileSmall = (android.widget.ImageView) bindings[11];
        this.editRepass = (android.widget.EditText) bindings[15];
        this.editSpeciality = (android.widget.EditText) bindings[3];
        this.editSpeciality.setTag(null);
        this.editUsername = (android.widget.EditText) bindings[4];
        this.editUsername.setTag(null);
        this.image = (com.pkmmte.view.CircularImageView) bindings[1];
        this.image.setTag(null);
        this.labelCity = (android.widget.TextView) bindings[19];
        this.labelCountry = (android.widget.TextView) bindings[21];
        this.labelLocation = (android.widget.TextView) bindings[17];
        this.labelSpeciality = (android.widget.TextView) bindings[12];
        this.labelState = (android.widget.TextView) bindings[20];
        this.lableDegree = (android.widget.TextView) bindings[16];
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.parentImage = (android.widget.FrameLayout) bindings[10];
        this.placesAutocomplete = (com.seatgeek.placesautocomplete.PlacesAutocompleteTextView) bindings[18];
        this.saveBtn = (android.widget.Button) bindings[22];
        this.spinGender = (android.widget.Spinner) bindings[13];
        this.state = (android.widget.EditText) bindings[8];
        this.state.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.user == variableId) {
            setUser((com.mdconsults.data.model.User) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setUser(@Nullable com.mdconsults.data.model.User User) {
        this.mUser = User;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.user);
        super.requestRebind();
    }
    @Nullable
    public com.mdconsults.data.model.User getUser() {
        return mUser;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String userCountry = null;
        java.lang.String userUsername = null;
        java.lang.String userName = null;
        java.lang.String userDegree = null;
        java.lang.String userCity = null;
        java.lang.String userEmail = null;
        java.lang.String userState = null;
        java.lang.String userSpeciality = null;
        com.mdconsults.data.model.User user = mUser;
        java.lang.String userImage = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (user != null) {
                    // read user.country
                    userCountry = user.getCountry();
                    // read user.username
                    userUsername = user.getUsername();
                    // read user.name
                    userName = user.getName();
                    // read user.degree
                    userDegree = user.getDegree();
                    // read user.city
                    userCity = user.getCity();
                    // read user.email
                    userEmail = user.getEmail();
                    // read user.state
                    userState = user.getState();
                    // read user.speciality
                    userSpeciality = user.getSpeciality();
                    // read user.image
                    userImage = user.getImage();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.city, userCity);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.country, userCountry);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.degree, userDegree);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.editEmail, userEmail);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.editName, userName);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.editSpeciality, userSpeciality);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.editUsername, userUsername);
            com.mdconsults.util.UiBinder.loadImagefromFile(this.image, userImage);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.state, userState);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ActivitySettingBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivitySettingBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ActivitySettingBinding>inflate(inflater, com.mdconsults.R.layout.activity_setting, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ActivitySettingBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivitySettingBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.mdconsults.R.layout.activity_setting, null, false), bindingComponent);
    }
    @NonNull
    public static ActivitySettingBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivitySettingBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/activity_setting_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ActivitySettingBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): user
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}
package com.mdconsults.authenticate;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class SignUpActivity_MembersInjector implements MembersInjector<SignUpActivity> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public SignUpActivity_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<SignUpActivity> create(Provider<ApiFactory> apiFactoryProvider) {
    return new SignUpActivity_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(SignUpActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      SignUpActivity instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

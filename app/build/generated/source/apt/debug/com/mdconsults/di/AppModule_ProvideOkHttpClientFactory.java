package com.mdconsults.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
  private final AppModule module;

  private final Provider<Cache> cacheProvider;

  public AppModule_ProvideOkHttpClientFactory(AppModule module, Provider<Cache> cacheProvider) {
    assert module != null;
    this.module = module;
    assert cacheProvider != null;
    this.cacheProvider = cacheProvider;
  }

  @Override
  public OkHttpClient get() {
    return Preconditions.checkNotNull(
        module.provideOkHttpClient(cacheProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<OkHttpClient> create(AppModule module, Provider<Cache> cacheProvider) {
    return new AppModule_ProvideOkHttpClientFactory(module, cacheProvider);
  }

  /** Proxies {@link AppModule#provideOkHttpClient(Cache)}. */
  public static OkHttpClient proxyProvideOkHttpClient(AppModule instance, Cache cache) {
    return instance.provideOkHttpClient(cache);
  }
}

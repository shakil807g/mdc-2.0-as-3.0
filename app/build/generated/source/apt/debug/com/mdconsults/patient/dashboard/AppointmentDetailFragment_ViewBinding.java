// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.dashboard;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import com.mdconsults.widget.FlowLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppointmentDetailFragment_ViewBinding implements Unbinder {
  private AppointmentDetailFragment target;

  @UiThread
  public AppointmentDetailFragment_ViewBinding(AppointmentDetailFragment target, View source) {
    this.target = target;

    target.txtGender = Utils.findRequiredViewAsType(source, R.id.txt_gender, "field 'txtGender'", TextView.class);
    target.txtLocation = Utils.findRequiredViewAsType(source, R.id.txt_location, "field 'txtLocation'", TextView.class);
    target.txtAge = Utils.findRequiredViewAsType(source, R.id.txt_age, "field 'txtAge'", TextView.class);
    target.txtDatetime = Utils.findRequiredViewAsType(source, R.id.txt_datetime, "field 'txtDatetime'", TextView.class);
    target.txtComment = Utils.findRequiredViewAsType(source, R.id.txt_comment, "field 'txtComment'", TextView.class);
    target.imageContainer = Utils.findRequiredViewAsType(source, R.id.image_container, "field 'imageContainer'", FlowLayout.class);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.dp_150 = res.getDimensionPixelSize(R.dimen.dp_150);
    target.dp_10 = res.getDimensionPixelSize(R.dimen.dp_10);
  }

  @Override
  @CallSuper
  public void unbind() {
    AppointmentDetailFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtGender = null;
    target.txtLocation = null;
    target.txtAge = null;
    target.txtDatetime = null;
    target.txtComment = null;
    target.imageContainer = null;
  }
}

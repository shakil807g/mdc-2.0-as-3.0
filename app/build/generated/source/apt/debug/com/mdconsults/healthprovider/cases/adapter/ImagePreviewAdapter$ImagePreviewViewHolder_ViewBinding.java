// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.cases.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ImagePreviewAdapter$ImagePreviewViewHolder_ViewBinding implements Unbinder {
  private ImagePreviewAdapter.ImagePreviewViewHolder target;

  @UiThread
  public ImagePreviewAdapter$ImagePreviewViewHolder_ViewBinding(ImagePreviewAdapter.ImagePreviewViewHolder target, View source) {
    this.target = target;

    target.ivImage = Utils.findRequiredViewAsType(source, R.id.ivImage, "field 'ivImage'", ImageView.class);
    target.cross = Utils.findRequiredViewAsType(source, R.id.cross, "field 'cross'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ImagePreviewAdapter.ImagePreviewViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivImage = null;
    target.cross = null;
  }
}

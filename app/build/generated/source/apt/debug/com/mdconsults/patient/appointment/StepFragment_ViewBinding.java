// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.appointment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StepFragment_ViewBinding implements Unbinder {
  private StepFragment target;

  private View view2131296312;

  @UiThread
  public StepFragment_ViewBinding(final StepFragment target, View source) {
    this.target = target;

    View view;
    target.editText = Utils.findRequiredViewAsType(source, R.id.editText, "field 'editText'", EditText.class);
    target.description = Utils.findRequiredViewAsType(source, R.id.description, "field 'description'", CardView.class);
    view = Utils.findRequiredView(source, R.id.camera_fab, "field 'cameraFab' and method 'onViewClicked'");
    target.cameraFab = Utils.castView(view, R.id.camera_fab, "field 'cameraFab'", FloatingActionButton.class);
    view2131296312 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.lvResults = Utils.findRequiredViewAsType(source, R.id.lvResults, "field 'lvResults'", ListView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StepFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.editText = null;
    target.description = null;
    target.cameraFab = null;
    target.lvResults = null;

    view2131296312.setOnClickListener(null);
    view2131296312 = null;
  }
}

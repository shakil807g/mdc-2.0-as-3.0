// Generated code from Butter Knife. Do not modify!
package com.mdconsults.widget;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActionBar_ViewBinding implements Unbinder {
  private ActionBar target;

  @UiThread
  public ActionBar_ViewBinding(ActionBar target) {
    this(target, target);
  }

  @UiThread
  public ActionBar_ViewBinding(ActionBar target, View source) {
    this.target = target;

    target.menu = Utils.findRequiredViewAsType(source, R.id.menu, "field 'menu'", ImageButton.class);
    target.profileImage = Utils.findRequiredViewAsType(source, R.id.profile_image, "field 'profileImage'", CircleImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActionBar target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.menu = null;
    target.profileImage = null;
  }
}

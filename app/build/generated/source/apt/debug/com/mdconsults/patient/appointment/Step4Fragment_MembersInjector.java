package com.mdconsults.patient.appointment;

import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.util.RxBus;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class Step4Fragment_MembersInjector implements MembersInjector<Step4Fragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  private final Provider<RxBus> rxBusProvider;

  public Step4Fragment_MembersInjector(
      Provider<ApiFactory> apiFactoryProvider, Provider<RxBus> rxBusProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
    assert rxBusProvider != null;
    this.rxBusProvider = rxBusProvider;
  }

  public static MembersInjector<Step4Fragment> create(
      Provider<ApiFactory> apiFactoryProvider, Provider<RxBus> rxBusProvider) {
    return new Step4Fragment_MembersInjector(apiFactoryProvider, rxBusProvider);
  }

  @Override
  public void injectMembers(Step4Fragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
    instance.rxBus = rxBusProvider.get();
  }

  public static void injectApiFactory(
      Step4Fragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectRxBus(Step4Fragment instance, Provider<RxBus> rxBusProvider) {
    instance.rxBus = rxBusProvider.get();
  }
}

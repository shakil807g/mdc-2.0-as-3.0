package com.mdconsults.data;

import com.mdconsults.data.local.Doa.MainAppointmentDetailDoa;
import com.mdconsults.data.local.Doa.MainAppointmentDoa;
import com.mdconsults.data.local.MDCDatabase;
import com.mdconsults.data.remote.ApiFactory;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MDCRepository_Factory implements Factory<MDCRepository> {
  private final Provider<ApiFactory> apiFactoryProvider;

  private final Provider<MDCDatabase> mdcDatabaseProvider;

  private final Provider<MainAppointmentDoa> mainAppointmentDoaProvider;

  private final Provider<MainAppointmentDetailDoa> mainAppointmentDetailDoaProvider;

  public MDCRepository_Factory(
      Provider<ApiFactory> apiFactoryProvider,
      Provider<MDCDatabase> mdcDatabaseProvider,
      Provider<MainAppointmentDoa> mainAppointmentDoaProvider,
      Provider<MainAppointmentDetailDoa> mainAppointmentDetailDoaProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
    assert mdcDatabaseProvider != null;
    this.mdcDatabaseProvider = mdcDatabaseProvider;
    assert mainAppointmentDoaProvider != null;
    this.mainAppointmentDoaProvider = mainAppointmentDoaProvider;
    assert mainAppointmentDetailDoaProvider != null;
    this.mainAppointmentDetailDoaProvider = mainAppointmentDetailDoaProvider;
  }

  @Override
  public MDCRepository get() {
    return new MDCRepository(
        apiFactoryProvider.get(),
        mdcDatabaseProvider.get(),
        mainAppointmentDoaProvider.get(),
        mainAppointmentDetailDoaProvider.get());
  }

  public static Factory<MDCRepository> create(
      Provider<ApiFactory> apiFactoryProvider,
      Provider<MDCDatabase> mdcDatabaseProvider,
      Provider<MainAppointmentDoa> mainAppointmentDoaProvider,
      Provider<MainAppointmentDetailDoa> mainAppointmentDetailDoaProvider) {
    return new MDCRepository_Factory(
        apiFactoryProvider,
        mdcDatabaseProvider,
        mainAppointmentDoaProvider,
        mainAppointmentDetailDoaProvider);
  }
}

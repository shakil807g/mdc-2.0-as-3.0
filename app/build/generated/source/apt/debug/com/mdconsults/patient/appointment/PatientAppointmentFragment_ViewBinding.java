// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.appointment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import com.stepstone.stepper.StepperLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PatientAppointmentFragment_ViewBinding implements Unbinder {
  private PatientAppointmentFragment target;

  @UiThread
  public PatientAppointmentFragment_ViewBinding(PatientAppointmentFragment target, View source) {
    this.target = target;

    target.stepperLayout = Utils.findRequiredViewAsType(source, R.id.stepperLayout, "field 'stepperLayout'", StepperLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PatientAppointmentFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.stepperLayout = null;
  }
}

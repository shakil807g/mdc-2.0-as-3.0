package com.mdconsults.data.local.Doa;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import com.mdconsults.data.remote.model.MainAppointmentDetail;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

public class MainAppointmentDetailDoa_Impl implements MainAppointmentDetailDoa {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfMainAppointmentDetail;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public MainAppointmentDetailDoa_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMainAppointmentDetail = new EntityInsertionAdapter<MainAppointmentDetail>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `MainAppointmentDetail`(`id`,`caseCode`,`fname`,`lname`,`gender`,`yAge`,`monthAge`,`mAge`,`diseaseId`,`locationId`,`history`,`question`,`city`,`formId`,`patientId`,`refererId`,`data`,`createdAt`,`updatedAt`,`deletedAt`,`username`,`speciality`,`name`,`email`,`country`,`state`,`affiliation`,`degree`,`password`,`rememberToken`,`image`,`licenseOwner`,`status`,`switchRole`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MainAppointmentDetail value) {
        if (value.id == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.id);
        }
        if (value.caseCode == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.caseCode);
        }
        if (value.fname == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.fname);
        }
        if (value.lname == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.lname);
        }
        if (value.gender == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.gender);
        }
        if (value.yAge == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.yAge);
        }
        if (value.monthAge == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.monthAge);
        }
        if (value.mAge == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.mAge);
        }
        if (value.diseaseId == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.diseaseId);
        }
        if (value.locationId == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.locationId);
        }
        if (value.history == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.history);
        }
        if (value.question == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.question);
        }
        if (value.city == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.city);
        }
        if (value.formId == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.formId);
        }
        if (value.patientId == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.patientId);
        }
        if (value.refererId == null) {
          stmt.bindNull(16);
        } else {
          stmt.bindString(16, value.refererId);
        }
        if (value.data == null) {
          stmt.bindNull(17);
        } else {
          stmt.bindString(17, value.data);
        }
        if (value.createdAt == null) {
          stmt.bindNull(18);
        } else {
          stmt.bindString(18, value.createdAt);
        }
        if (value.updatedAt == null) {
          stmt.bindNull(19);
        } else {
          stmt.bindString(19, value.updatedAt);
        }
        if (value.deletedAt == null) {
          stmt.bindNull(20);
        } else {
          stmt.bindString(20, value.deletedAt);
        }
        if (value.username == null) {
          stmt.bindNull(21);
        } else {
          stmt.bindString(21, value.username);
        }
        if (value.speciality == null) {
          stmt.bindNull(22);
        } else {
          stmt.bindString(22, value.speciality);
        }
        if (value.name == null) {
          stmt.bindNull(23);
        } else {
          stmt.bindString(23, value.name);
        }
        if (value.email == null) {
          stmt.bindNull(24);
        } else {
          stmt.bindString(24, value.email);
        }
        if (value.country == null) {
          stmt.bindNull(25);
        } else {
          stmt.bindString(25, value.country);
        }
        if (value.state == null) {
          stmt.bindNull(26);
        } else {
          stmt.bindString(26, value.state);
        }
        if (value.affiliation == null) {
          stmt.bindNull(27);
        } else {
          stmt.bindString(27, value.affiliation);
        }
        if (value.degree == null) {
          stmt.bindNull(28);
        } else {
          stmt.bindString(28, value.degree);
        }
        if (value.password == null) {
          stmt.bindNull(29);
        } else {
          stmt.bindString(29, value.password);
        }
        if (value.rememberToken == null) {
          stmt.bindNull(30);
        } else {
          stmt.bindString(30, value.rememberToken);
        }
        if (value.image == null) {
          stmt.bindNull(31);
        } else {
          stmt.bindString(31, value.image);
        }
        if (value.licenseOwner == null) {
          stmt.bindNull(32);
        } else {
          stmt.bindString(32, value.licenseOwner);
        }
        if (value.status == null) {
          stmt.bindNull(33);
        } else {
          stmt.bindString(33, value.status);
        }
        if (value.switchRole == null) {
          stmt.bindNull(34);
        } else {
          stmt.bindString(34, value.switchRole);
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM MainAppointmentDetail WHERE id = ?";
        return _query;
      }
    };
  }

  @Override
  public void insertAll(List<MainAppointmentDetail> mainAppointments) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMainAppointmentDetail.insert(mainAppointments);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(String id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (id == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, id);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public List<MainAppointmentDetail> getMainAppointmentDetail(String id) {
    final String _sql = "SELECT * FROM MainAppointmentDetail WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfCaseCode = _cursor.getColumnIndexOrThrow("caseCode");
      final int _cursorIndexOfFname = _cursor.getColumnIndexOrThrow("fname");
      final int _cursorIndexOfLname = _cursor.getColumnIndexOrThrow("lname");
      final int _cursorIndexOfGender = _cursor.getColumnIndexOrThrow("gender");
      final int _cursorIndexOfYAge = _cursor.getColumnIndexOrThrow("yAge");
      final int _cursorIndexOfMonthAge = _cursor.getColumnIndexOrThrow("monthAge");
      final int _cursorIndexOfMAge = _cursor.getColumnIndexOrThrow("mAge");
      final int _cursorIndexOfDiseaseId = _cursor.getColumnIndexOrThrow("diseaseId");
      final int _cursorIndexOfLocationId = _cursor.getColumnIndexOrThrow("locationId");
      final int _cursorIndexOfHistory = _cursor.getColumnIndexOrThrow("history");
      final int _cursorIndexOfQuestion = _cursor.getColumnIndexOrThrow("question");
      final int _cursorIndexOfCity = _cursor.getColumnIndexOrThrow("city");
      final int _cursorIndexOfFormId = _cursor.getColumnIndexOrThrow("formId");
      final int _cursorIndexOfPatientId = _cursor.getColumnIndexOrThrow("patientId");
      final int _cursorIndexOfRefererId = _cursor.getColumnIndexOrThrow("refererId");
      final int _cursorIndexOfData = _cursor.getColumnIndexOrThrow("data");
      final int _cursorIndexOfCreatedAt = _cursor.getColumnIndexOrThrow("createdAt");
      final int _cursorIndexOfUpdatedAt = _cursor.getColumnIndexOrThrow("updatedAt");
      final int _cursorIndexOfDeletedAt = _cursor.getColumnIndexOrThrow("deletedAt");
      final int _cursorIndexOfUsername = _cursor.getColumnIndexOrThrow("username");
      final int _cursorIndexOfSpeciality = _cursor.getColumnIndexOrThrow("speciality");
      final int _cursorIndexOfName = _cursor.getColumnIndexOrThrow("name");
      final int _cursorIndexOfEmail = _cursor.getColumnIndexOrThrow("email");
      final int _cursorIndexOfCountry = _cursor.getColumnIndexOrThrow("country");
      final int _cursorIndexOfState = _cursor.getColumnIndexOrThrow("state");
      final int _cursorIndexOfAffiliation = _cursor.getColumnIndexOrThrow("affiliation");
      final int _cursorIndexOfDegree = _cursor.getColumnIndexOrThrow("degree");
      final int _cursorIndexOfPassword = _cursor.getColumnIndexOrThrow("password");
      final int _cursorIndexOfRememberToken = _cursor.getColumnIndexOrThrow("rememberToken");
      final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
      final int _cursorIndexOfLicenseOwner = _cursor.getColumnIndexOrThrow("licenseOwner");
      final int _cursorIndexOfStatus = _cursor.getColumnIndexOrThrow("status");
      final int _cursorIndexOfSwitchRole = _cursor.getColumnIndexOrThrow("switchRole");
      final List<MainAppointmentDetail> _result = new ArrayList<MainAppointmentDetail>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final MainAppointmentDetail _item;
        _item = new MainAppointmentDetail();
        _item.id = _cursor.getString(_cursorIndexOfId);
        _item.caseCode = _cursor.getString(_cursorIndexOfCaseCode);
        _item.fname = _cursor.getString(_cursorIndexOfFname);
        _item.lname = _cursor.getString(_cursorIndexOfLname);
        _item.gender = _cursor.getString(_cursorIndexOfGender);
        _item.yAge = _cursor.getString(_cursorIndexOfYAge);
        _item.monthAge = _cursor.getString(_cursorIndexOfMonthAge);
        _item.mAge = _cursor.getString(_cursorIndexOfMAge);
        _item.diseaseId = _cursor.getString(_cursorIndexOfDiseaseId);
        _item.locationId = _cursor.getString(_cursorIndexOfLocationId);
        _item.history = _cursor.getString(_cursorIndexOfHistory);
        _item.question = _cursor.getString(_cursorIndexOfQuestion);
        _item.city = _cursor.getString(_cursorIndexOfCity);
        _item.formId = _cursor.getString(_cursorIndexOfFormId);
        _item.patientId = _cursor.getString(_cursorIndexOfPatientId);
        _item.refererId = _cursor.getString(_cursorIndexOfRefererId);
        _item.data = _cursor.getString(_cursorIndexOfData);
        _item.createdAt = _cursor.getString(_cursorIndexOfCreatedAt);
        _item.updatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
        _item.deletedAt = _cursor.getString(_cursorIndexOfDeletedAt);
        _item.username = _cursor.getString(_cursorIndexOfUsername);
        _item.speciality = _cursor.getString(_cursorIndexOfSpeciality);
        _item.name = _cursor.getString(_cursorIndexOfName);
        _item.email = _cursor.getString(_cursorIndexOfEmail);
        _item.country = _cursor.getString(_cursorIndexOfCountry);
        _item.state = _cursor.getString(_cursorIndexOfState);
        _item.affiliation = _cursor.getString(_cursorIndexOfAffiliation);
        _item.degree = _cursor.getString(_cursorIndexOfDegree);
        _item.password = _cursor.getString(_cursorIndexOfPassword);
        _item.rememberToken = _cursor.getString(_cursorIndexOfRememberToken);
        _item.image = _cursor.getString(_cursorIndexOfImage);
        _item.licenseOwner = _cursor.getString(_cursorIndexOfLicenseOwner);
        _item.status = _cursor.getString(_cursorIndexOfStatus);
        _item.switchRole = _cursor.getString(_cursorIndexOfSwitchRole);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}

package com.mdconsults.di;

import android.app.Application;
import android.content.SharedPreferences;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvidesSharedPreferencesFactory
    implements Factory<SharedPreferences> {
  private final AppModule module;

  private final Provider<Application> applicationProvider;

  public AppModule_ProvidesSharedPreferencesFactory(
      AppModule module, Provider<Application> applicationProvider) {
    assert module != null;
    this.module = module;
    assert applicationProvider != null;
    this.applicationProvider = applicationProvider;
  }

  @Override
  public SharedPreferences get() {
    return Preconditions.checkNotNull(
        module.providesSharedPreferences(applicationProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<SharedPreferences> create(
      AppModule module, Provider<Application> applicationProvider) {
    return new AppModule_ProvidesSharedPreferencesFactory(module, applicationProvider);
  }

  /** Proxies {@link AppModule#providesSharedPreferences(Application)}. */
  public static SharedPreferences proxyProvidesSharedPreferences(
      AppModule instance, Application application) {
    return instance.providesSharedPreferences(application);
  }
}

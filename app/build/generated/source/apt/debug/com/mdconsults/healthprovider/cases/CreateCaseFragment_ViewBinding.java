// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.cases;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import com.mdconsults.widget.HorizontalFlowLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateCaseFragment_ViewBinding implements Unbinder {
  private CreateCaseFragment target;

  private View view2131296589;

  private View view2131296312;

  private View view2131296306;

  private View view2131296646;

  @UiThread
  public CreateCaseFragment_ViewBinding(final CreateCaseFragment target, View source) {
    this.target = target;

    View view;
    target.lvResults = Utils.findRequiredViewAsType(source, R.id.lvResults, "field 'lvResults'", RecyclerView.class);
    target.mainScrollview = Utils.findRequiredViewAsType(source, R.id.main_scrollview, "field 'mainScrollview'", NestedScrollView.class);
    target.txtPatientId = Utils.findRequiredViewAsType(source, R.id.txt_patient_id, "field 'txtPatientId'", EditText.class);
    target.txtFirstname = Utils.findRequiredViewAsType(source, R.id.txt_firstname, "field 'txtFirstname'", EditText.class);
    target.txtLastname = Utils.findRequiredViewAsType(source, R.id.txt_lastname, "field 'txtLastname'", EditText.class);
    target.txtAgeYears = Utils.findRequiredViewAsType(source, R.id.txt_age_years, "field 'txtAgeYears'", EditText.class);
    target.txtAgeMonth = Utils.findRequiredViewAsType(source, R.id.txt_age_month, "field 'txtAgeMonth'", EditText.class);
    target.txtCurrentComplain = Utils.findRequiredViewAsType(source, R.id.txt_current_complain, "field 'txtCurrentComplain'", EditText.class);
    target.txtPastHistory = Utils.findRequiredViewAsType(source, R.id.txt_past_history, "field 'txtPastHistory'", EditText.class);
    target.txtComments = Utils.findRequiredViewAsType(source, R.id.txt_comments, "field 'txtComments'", EditText.class);
    target.spinLocation = Utils.findRequiredViewAsType(source, R.id.spin_location, "field 'spinLocation'", Spinner.class);
    target.referrerItems = Utils.findRequiredViewAsType(source, R.id.referrerItems, "field 'referrerItems'", HorizontalFlowLayout.class);
    view = Utils.findRequiredView(source, R.id.referview, "field 'referview' and method 'RefereItemSelected'");
    target.referview = Utils.castView(view, R.id.referview, "field 'referview'", Spinner.class);
    view2131296589 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.RefereItemSelected(Utils.castParam(p0, "onItemSelected", 0, "RefereItemSelected", 0, Spinner.class), p2);
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    view = Utils.findRequiredView(source, R.id.camera_fab, "method 'onViewClicked'");
    view2131296312 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_create_case, "method 'CreateCaseClick'");
    view2131296306 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.CreateCaseClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.spin_gender, "method 'spinnerItemSelected'");
    view2131296646 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.spinnerItemSelected(Utils.castParam(p0, "onItemSelected", 0, "spinnerItemSelected", 0, Spinner.class), p2);
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });

    Context context = source.getContext();
    Resources res = context.getResources();
    target.listviewSpace = res.getDimensionPixelSize(R.dimen.item_space_small);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateCaseFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.lvResults = null;
    target.mainScrollview = null;
    target.txtPatientId = null;
    target.txtFirstname = null;
    target.txtLastname = null;
    target.txtAgeYears = null;
    target.txtAgeMonth = null;
    target.txtCurrentComplain = null;
    target.txtPastHistory = null;
    target.txtComments = null;
    target.spinLocation = null;
    target.referrerItems = null;
    target.referview = null;

    ((AdapterView<?>) view2131296589).setOnItemSelectedListener(null);
    view2131296589 = null;
    view2131296312.setOnClickListener(null);
    view2131296312 = null;
    view2131296306.setOnClickListener(null);
    view2131296306 = null;
    ((AdapterView<?>) view2131296646).setOnItemSelectedListener(null);
    view2131296646 = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.group;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GroupDetailFragment_ViewBinding implements Unbinder {
  private GroupDetailFragment target;

  @UiThread
  public GroupDetailFragment_ViewBinding(GroupDetailFragment target, View source) {
    this.target = target;

    target.txtGroupMembers = Utils.findRequiredViewAsType(source, R.id.txt_group_members, "field 'txtGroupMembers'", TextView.class);
    target.txtGroupSpeciality = Utils.findRequiredViewAsType(source, R.id.txt_group_speciality, "field 'txtGroupSpeciality'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GroupDetailFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtGroupMembers = null;
    target.txtGroupSpeciality = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import com.mdconsults.widget.ActionBar;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PatientMainActivity_ViewBinding implements Unbinder {
  private PatientMainActivity target;

  private View view2131296635;

  private View view2131296628;

  @UiThread
  public PatientMainActivity_ViewBinding(PatientMainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PatientMainActivity_ViewBinding(final PatientMainActivity target, View source) {
    this.target = target;

    View view;
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
    target.darwerList = Utils.findRequiredViewAsType(source, R.id.darwerList, "field 'darwerList'", ListView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", ActionBar.class);
    target.username = Utils.findRequiredViewAsType(source, R.id.username, "field 'username'", TextView.class);
    target.drawerPic = Utils.findRequiredViewAsType(source, R.id.drawer_pic, "field 'drawerPic'", CircleImageView.class);
    view = Utils.findRequiredView(source, R.id.signout, "method 'signOut'");
    view2131296635 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.signOut();
      }
    });
    view = Utils.findRequiredView(source, R.id.setting, "method 'setting'");
    view2131296628 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.setting();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PatientMainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.drawerLayout = null;
    target.darwerList = null;
    target.toolbar = null;
    target.username = null;
    target.drawerPic = null;

    view2131296635.setOnClickListener(null);
    view2131296635 = null;
    view2131296628.setOnClickListener(null);
    view2131296628 = null;
  }
}

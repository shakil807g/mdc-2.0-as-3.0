package com.mdconsults.healthprovider.schedule;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ScheduleFragment_MembersInjector implements MembersInjector<ScheduleFragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public ScheduleFragment_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<ScheduleFragment> create(Provider<ApiFactory> apiFactoryProvider) {
    return new ScheduleFragment_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(ScheduleFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      ScheduleFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

package com.mdconsults.patient.dashboard;

import com.mdconsults.data.MDCRepository;
import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.util.RxBus;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PatientMainFragment_MembersInjector
    implements MembersInjector<PatientMainFragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  private final Provider<RxBus> rxBusProvider;

  private final Provider<MDCRepository> repositoryProvider;

  public PatientMainFragment_MembersInjector(
      Provider<ApiFactory> apiFactoryProvider,
      Provider<RxBus> rxBusProvider,
      Provider<MDCRepository> repositoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
    assert rxBusProvider != null;
    this.rxBusProvider = rxBusProvider;
    assert repositoryProvider != null;
    this.repositoryProvider = repositoryProvider;
  }

  public static MembersInjector<PatientMainFragment> create(
      Provider<ApiFactory> apiFactoryProvider,
      Provider<RxBus> rxBusProvider,
      Provider<MDCRepository> repositoryProvider) {
    return new PatientMainFragment_MembersInjector(
        apiFactoryProvider, rxBusProvider, repositoryProvider);
  }

  @Override
  public void injectMembers(PatientMainFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
    instance.rxBus = rxBusProvider.get();
    instance.repository = repositoryProvider.get();
  }

  public static void injectApiFactory(
      PatientMainFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectRxBus(PatientMainFragment instance, Provider<RxBus> rxBusProvider) {
    instance.rxBus = rxBusProvider.get();
  }

  public static void injectRepository(
      PatientMainFragment instance, Provider<MDCRepository> repositoryProvider) {
    instance.repository = repositoryProvider.get();
  }
}

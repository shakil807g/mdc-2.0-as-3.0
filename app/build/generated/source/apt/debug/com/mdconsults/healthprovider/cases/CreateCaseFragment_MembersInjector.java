package com.mdconsults.healthprovider.cases;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CreateCaseFragment_MembersInjector
    implements MembersInjector<CreateCaseFragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  private final Provider<OkHttpClient> okHttpClientProvider;

  public CreateCaseFragment_MembersInjector(
      Provider<ApiFactory> apiFactoryProvider, Provider<OkHttpClient> okHttpClientProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
    assert okHttpClientProvider != null;
    this.okHttpClientProvider = okHttpClientProvider;
  }

  public static MembersInjector<CreateCaseFragment> create(
      Provider<ApiFactory> apiFactoryProvider, Provider<OkHttpClient> okHttpClientProvider) {
    return new CreateCaseFragment_MembersInjector(apiFactoryProvider, okHttpClientProvider);
  }

  @Override
  public void injectMembers(CreateCaseFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
    instance.okHttpClient = okHttpClientProvider.get();
  }

  public static void injectApiFactory(
      CreateCaseFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectOkHttpClient(
      CreateCaseFragment instance, Provider<OkHttpClient> okHttpClientProvider) {
    instance.okHttpClient = okHttpClientProvider.get();
  }
}

package com.mdconsults.healthprovider.cases;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CaseDetailFragment_MembersInjector
    implements MembersInjector<CaseDetailFragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public CaseDetailFragment_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<CaseDetailFragment> create(
      Provider<ApiFactory> apiFactoryProvider) {
    return new CaseDetailFragment_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(CaseDetailFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      CaseDetailFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

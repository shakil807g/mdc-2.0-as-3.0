// Generated code from Butter Knife. Do not modify!
package com.mdconsults.authenticate;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignUpActivity_ViewBinding implements Unbinder {
  private SignUpActivity target;

  private View view2131296636;

  private View view2131296564;

  private View view2131296424;

  @UiThread
  public SignUpActivity_ViewBinding(SignUpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignUpActivity_ViewBinding(final SignUpActivity target, View source) {
    this.target = target;

    View view;
    target.checkBox = Utils.findRequiredViewAsType(source, R.id.checkBox, "field 'checkBox'", CheckBox.class);
    target.password = Utils.findRequiredViewAsType(source, R.id.password, "field 'password'", EditText.class);
    target.fullname = Utils.findRequiredViewAsType(source, R.id.fullname, "field 'fullname'", EditText.class);
    view = Utils.findRequiredView(source, R.id.signup, "field 'signup' and method 'onViewClicked'");
    target.signup = Utils.castView(view, R.id.signup, "field 'signup'", Button.class);
    view2131296636 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    target.repassword = Utils.findRequiredViewAsType(source, R.id.repassword, "field 'repassword'", EditText.class);
    target.username = Utils.findRequiredViewAsType(source, R.id.username, "field 'username'", EditText.class);
    view = Utils.findRequiredView(source, R.id.patient_radio, "method 'onCheckChange'");
    view2131296564 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onCheckChange(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.health_provider, "method 'onCheckChange'");
    view2131296424 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onCheckChange(p0, p1);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SignUpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.checkBox = null;
    target.password = null;
    target.fullname = null;
    target.signup = null;
    target.email = null;
    target.repassword = null;
    target.username = null;

    view2131296636.setOnClickListener(null);
    view2131296636 = null;
    ((CompoundButton) view2131296564).setOnCheckedChangeListener(null);
    view2131296564 = null;
    ((CompoundButton) view2131296424).setOnCheckedChangeListener(null);
    view2131296424 = null;
  }
}

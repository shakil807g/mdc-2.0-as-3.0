package com.mdconsults.data.local;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import com.mdconsults.data.local.Doa.MainAppointmentDetailDoa;
import com.mdconsults.data.local.Doa.MainAppointmentDetailDoa_Impl;
import com.mdconsults.data.local.Doa.MainAppointmentDetailImageDoa;
import com.mdconsults.data.local.Doa.MainAppointmentDetailImageDoa_Impl;
import com.mdconsults.data.local.Doa.MainAppointmentDoa;
import com.mdconsults.data.local.Doa.MainAppointmentDoa_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.util.HashMap;
import java.util.HashSet;

public class MDCDatabase_Impl extends MDCDatabase {
  private volatile MainAppointmentDoa _mainAppointmentDoa;

  private volatile MainAppointmentDetailDoa _mainAppointmentDetailDoa;

  private volatile MainAppointmentDetailImageDoa _mainAppointmentDetailImageDoa;

  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate() {
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `MainAppointment` (`date` TEXT, `id` TEXT, `record_id` TEXT, `name` TEXT, `start_time` TEXT, `end_time` TEXT, `start_date` TEXT, `image` TEXT, `room_url` TEXT, `patient_id` TEXT, PRIMARY KEY(`record_id`, `id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `MainAppointmentDetail` (`id` TEXT, `caseCode` TEXT, `fname` TEXT, `lname` TEXT, `gender` TEXT, `yAge` TEXT, `monthAge` TEXT, `mAge` TEXT, `diseaseId` TEXT, `locationId` TEXT, `history` TEXT, `question` TEXT, `city` TEXT, `formId` TEXT, `patientId` TEXT, `refererId` TEXT, `data` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `deletedAt` TEXT, `username` TEXT, `speciality` TEXT, `name` TEXT, `email` TEXT, `country` TEXT, `state` TEXT, `affiliation` TEXT, `degree` TEXT, `password` TEXT, `rememberToken` TEXT, `image` TEXT, `licenseOwner` TEXT, `status` TEXT, `switchRole` TEXT, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `MainAppointmentDetailImage` (`id` TEXT, `recordId` TEXT, `image` TEXT, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"23c1c92d873a3b51fed0db1b757513a4\")");
      }

      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `MainAppointment`");
        _db.execSQL("DROP TABLE IF EXISTS `MainAppointmentDetail`");
        _db.execSQL("DROP TABLE IF EXISTS `MainAppointmentDetailImage`");
      }

      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsMainAppointment = new HashMap<String, TableInfo.Column>(10);
        _columnsMainAppointment.put("date", new TableInfo.Column("date", "TEXT", false, 0));
        _columnsMainAppointment.put("id", new TableInfo.Column("id", "TEXT", false, 2));
        _columnsMainAppointment.put("record_id", new TableInfo.Column("record_id", "TEXT", false, 1));
        _columnsMainAppointment.put("name", new TableInfo.Column("name", "TEXT", false, 0));
        _columnsMainAppointment.put("start_time", new TableInfo.Column("start_time", "TEXT", false, 0));
        _columnsMainAppointment.put("end_time", new TableInfo.Column("end_time", "TEXT", false, 0));
        _columnsMainAppointment.put("start_date", new TableInfo.Column("start_date", "TEXT", false, 0));
        _columnsMainAppointment.put("image", new TableInfo.Column("image", "TEXT", false, 0));
        _columnsMainAppointment.put("room_url", new TableInfo.Column("room_url", "TEXT", false, 0));
        _columnsMainAppointment.put("patient_id", new TableInfo.Column("patient_id", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMainAppointment = new HashSet<TableInfo.ForeignKey>(0);
        final TableInfo _infoMainAppointment = new TableInfo("MainAppointment", _columnsMainAppointment, _foreignKeysMainAppointment);
        final TableInfo _existingMainAppointment = TableInfo.read(_db, "MainAppointment");
        if (! _infoMainAppointment.equals(_existingMainAppointment)) {
          throw new IllegalStateException("Migration didn't properly handle MainAppointment(com.mdconsults.data.model.MainAppointment).\n"
                  + " Expected:\n" + _infoMainAppointment + "\n"
                  + " Found:\n" + _existingMainAppointment);
        }
        final HashMap<String, TableInfo.Column> _columnsMainAppointmentDetail = new HashMap<String, TableInfo.Column>(34);
        _columnsMainAppointmentDetail.put("id", new TableInfo.Column("id", "TEXT", false, 1));
        _columnsMainAppointmentDetail.put("caseCode", new TableInfo.Column("caseCode", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("fname", new TableInfo.Column("fname", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("lname", new TableInfo.Column("lname", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("gender", new TableInfo.Column("gender", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("yAge", new TableInfo.Column("yAge", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("monthAge", new TableInfo.Column("monthAge", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("mAge", new TableInfo.Column("mAge", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("diseaseId", new TableInfo.Column("diseaseId", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("locationId", new TableInfo.Column("locationId", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("history", new TableInfo.Column("history", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("question", new TableInfo.Column("question", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("city", new TableInfo.Column("city", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("formId", new TableInfo.Column("formId", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("patientId", new TableInfo.Column("patientId", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("refererId", new TableInfo.Column("refererId", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("data", new TableInfo.Column("data", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("createdAt", new TableInfo.Column("createdAt", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("updatedAt", new TableInfo.Column("updatedAt", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("deletedAt", new TableInfo.Column("deletedAt", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("username", new TableInfo.Column("username", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("speciality", new TableInfo.Column("speciality", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("name", new TableInfo.Column("name", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("email", new TableInfo.Column("email", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("country", new TableInfo.Column("country", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("state", new TableInfo.Column("state", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("affiliation", new TableInfo.Column("affiliation", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("degree", new TableInfo.Column("degree", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("password", new TableInfo.Column("password", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("rememberToken", new TableInfo.Column("rememberToken", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("image", new TableInfo.Column("image", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("licenseOwner", new TableInfo.Column("licenseOwner", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("status", new TableInfo.Column("status", "TEXT", false, 0));
        _columnsMainAppointmentDetail.put("switchRole", new TableInfo.Column("switchRole", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMainAppointmentDetail = new HashSet<TableInfo.ForeignKey>(0);
        final TableInfo _infoMainAppointmentDetail = new TableInfo("MainAppointmentDetail", _columnsMainAppointmentDetail, _foreignKeysMainAppointmentDetail);
        final TableInfo _existingMainAppointmentDetail = TableInfo.read(_db, "MainAppointmentDetail");
        if (! _infoMainAppointmentDetail.equals(_existingMainAppointmentDetail)) {
          throw new IllegalStateException("Migration didn't properly handle MainAppointmentDetail(com.mdconsults.data.remote.model.MainAppointmentDetail).\n"
                  + " Expected:\n" + _infoMainAppointmentDetail + "\n"
                  + " Found:\n" + _existingMainAppointmentDetail);
        }
        final HashMap<String, TableInfo.Column> _columnsMainAppointmentDetailImage = new HashMap<String, TableInfo.Column>(3);
        _columnsMainAppointmentDetailImage.put("id", new TableInfo.Column("id", "TEXT", false, 1));
        _columnsMainAppointmentDetailImage.put("recordId", new TableInfo.Column("recordId", "TEXT", false, 0));
        _columnsMainAppointmentDetailImage.put("image", new TableInfo.Column("image", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMainAppointmentDetailImage = new HashSet<TableInfo.ForeignKey>(0);
        final TableInfo _infoMainAppointmentDetailImage = new TableInfo("MainAppointmentDetailImage", _columnsMainAppointmentDetailImage, _foreignKeysMainAppointmentDetailImage);
        final TableInfo _existingMainAppointmentDetailImage = TableInfo.read(_db, "MainAppointmentDetailImage");
        if (! _infoMainAppointmentDetailImage.equals(_existingMainAppointmentDetailImage)) {
          throw new IllegalStateException("Migration didn't properly handle MainAppointmentDetailImage(com.mdconsults.data.remote.model.MainAppointmentDetailImage).\n"
                  + " Expected:\n" + _infoMainAppointmentDetailImage + "\n"
                  + " Found:\n" + _existingMainAppointmentDetailImage);
        }
      }
    }, "23c1c92d873a3b51fed0db1b757513a4");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .version(1)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "MainAppointment","MainAppointmentDetail","MainAppointmentDetailImage");
  }

  @Override
  public MainAppointmentDoa mainAppointmentDoa() {
    if (_mainAppointmentDoa != null) {
      return _mainAppointmentDoa;
    } else {
      synchronized(this) {
        if(_mainAppointmentDoa == null) {
          _mainAppointmentDoa = new MainAppointmentDoa_Impl(this);
        }
        return _mainAppointmentDoa;
      }
    }
  }

  @Override
  public MainAppointmentDetailDoa mainAppointmentDetailDoa() {
    if (_mainAppointmentDetailDoa != null) {
      return _mainAppointmentDetailDoa;
    } else {
      synchronized(this) {
        if(_mainAppointmentDetailDoa == null) {
          _mainAppointmentDetailDoa = new MainAppointmentDetailDoa_Impl(this);
        }
        return _mainAppointmentDetailDoa;
      }
    }
  }

  @Override
  public MainAppointmentDetailImageDoa mainAppointmentDetailImageDoa() {
    if (_mainAppointmentDetailImageDoa != null) {
      return _mainAppointmentDetailImageDoa;
    } else {
      synchronized(this) {
        if(_mainAppointmentDetailImageDoa == null) {
          _mainAppointmentDetailImageDoa = new MainAppointmentDetailImageDoa_Impl(this);
        }
        return _mainAppointmentDetailImageDoa;
      }
    }
  }
}

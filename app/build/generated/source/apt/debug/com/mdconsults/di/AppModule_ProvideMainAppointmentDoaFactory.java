package com.mdconsults.di;

import com.mdconsults.data.local.Doa.MainAppointmentDoa;
import com.mdconsults.data.local.MDCDatabase;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideMainAppointmentDoaFactory
    implements Factory<MainAppointmentDoa> {
  private final AppModule module;

  private final Provider<MDCDatabase> dbProvider;

  public AppModule_ProvideMainAppointmentDoaFactory(
      AppModule module, Provider<MDCDatabase> dbProvider) {
    assert module != null;
    this.module = module;
    assert dbProvider != null;
    this.dbProvider = dbProvider;
  }

  @Override
  public MainAppointmentDoa get() {
    return Preconditions.checkNotNull(
        module.provideMainAppointmentDoa(dbProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MainAppointmentDoa> create(
      AppModule module, Provider<MDCDatabase> dbProvider) {
    return new AppModule_ProvideMainAppointmentDoaFactory(module, dbProvider);
  }

  /** Proxies {@link AppModule#provideMainAppointmentDoa(MDCDatabase)}. */
  public static MainAppointmentDoa proxyProvideMainAppointmentDoa(
      AppModule instance, MDCDatabase db) {
    return instance.provideMainAppointmentDoa(db);
  }
}

package com.mdconsults.healthprovider.group;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewGroupDailog_MembersInjector implements MembersInjector<NewGroupDailog> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public NewGroupDailog_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<NewGroupDailog> create(Provider<ApiFactory> apiFactoryProvider) {
    return new NewGroupDailog_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(NewGroupDailog instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      NewGroupDailog instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

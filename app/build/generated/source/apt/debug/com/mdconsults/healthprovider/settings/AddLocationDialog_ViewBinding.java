// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.settings;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddLocationDialog_ViewBinding implements Unbinder {
  private AddLocationDialog target;

  private View view2131296600;

  @UiThread
  public AddLocationDialog_ViewBinding(final AddLocationDialog target, View source) {
    this.target = target;

    View view;
    target.countrySpin = Utils.findRequiredViewAsType(source, R.id.country_spin, "field 'countrySpin'", Spinner.class);
    target.citySpin = Utils.findRequiredViewAsType(source, R.id.city_spin, "field 'citySpin'", Spinner.class);
    target.stateSpin = Utils.findRequiredViewAsType(source, R.id.state_spin, "field 'stateSpin'", Spinner.class);
    target.locationEdit = Utils.findRequiredViewAsType(source, R.id.location_edit, "field 'locationEdit'", EditText.class);
    target.addressEdit = Utils.findRequiredViewAsType(source, R.id.address_edit, "field 'addressEdit'", EditText.class);
    view = Utils.findRequiredView(source, R.id.save_btn, "field 'saveBtn' and method 'onViewClicked'");
    target.saveBtn = Utils.castView(view, R.id.save_btn, "field 'saveBtn'", Button.class);
    view2131296600 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddLocationDialog target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.countrySpin = null;
    target.citySpin = null;
    target.stateSpin = null;
    target.locationEdit = null;
    target.addressEdit = null;
    target.saveBtn = null;

    view2131296600.setOnClickListener(null);
    view2131296600 = null;
  }
}

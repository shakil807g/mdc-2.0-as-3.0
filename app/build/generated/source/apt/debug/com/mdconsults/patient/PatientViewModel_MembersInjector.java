package com.mdconsults.patient;

import com.mdconsults.data.MDCRepository;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PatientViewModel_MembersInjector implements MembersInjector<PatientViewModel> {
  private final Provider<MDCRepository> repositoryProvider;

  public PatientViewModel_MembersInjector(Provider<MDCRepository> repositoryProvider) {
    assert repositoryProvider != null;
    this.repositoryProvider = repositoryProvider;
  }

  public static MembersInjector<PatientViewModel> create(
      Provider<MDCRepository> repositoryProvider) {
    return new PatientViewModel_MembersInjector(repositoryProvider);
  }

  @Override
  public void injectMembers(PatientViewModel instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.repository = repositoryProvider.get();
  }

  public static void injectRepository(
      PatientViewModel instance, Provider<MDCRepository> repositoryProvider) {
    instance.repository = repositoryProvider.get();
  }
}

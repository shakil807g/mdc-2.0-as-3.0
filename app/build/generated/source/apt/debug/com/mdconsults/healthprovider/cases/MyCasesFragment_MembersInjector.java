package com.mdconsults.healthprovider.cases;

import com.mdconsults.data.remote.ApiFactory;
import com.mdconsults.util.RxBus;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MyCasesFragment_MembersInjector implements MembersInjector<MyCasesFragment> {
  private final Provider<RxBus> rxBusProvider;

  private final Provider<ApiFactory> apiFactoryProvider;

  public MyCasesFragment_MembersInjector(
      Provider<RxBus> rxBusProvider, Provider<ApiFactory> apiFactoryProvider) {
    assert rxBusProvider != null;
    this.rxBusProvider = rxBusProvider;
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<MyCasesFragment> create(
      Provider<RxBus> rxBusProvider, Provider<ApiFactory> apiFactoryProvider) {
    return new MyCasesFragment_MembersInjector(rxBusProvider, apiFactoryProvider);
  }

  @Override
  public void injectMembers(MyCasesFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.rxBus = rxBusProvider.get();
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectRxBus(MyCasesFragment instance, Provider<RxBus> rxBusProvider) {
    instance.rxBus = rxBusProvider.get();
  }

  public static void injectApiFactory(
      MyCasesFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

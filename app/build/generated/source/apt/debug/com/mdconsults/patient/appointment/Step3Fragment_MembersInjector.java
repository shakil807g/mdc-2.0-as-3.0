package com.mdconsults.patient.appointment;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class Step3Fragment_MembersInjector implements MembersInjector<Step3Fragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public Step3Fragment_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<Step3Fragment> create(Provider<ApiFactory> apiFactoryProvider) {
    return new Step3Fragment_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(Step3Fragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      Step3Fragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.dashboard.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainAppointmentAdapter$MainAppointmentViewHolder_ViewBinding implements Unbinder {
  private MainAppointmentAdapter.MainAppointmentViewHolder target;

  @UiThread
  public MainAppointmentAdapter$MainAppointmentViewHolder_ViewBinding(MainAppointmentAdapter.MainAppointmentViewHolder target, View source) {
    this.target = target;

    target.docName = Utils.findRequiredViewAsType(source, R.id.doc_name, "field 'docName'", TextView.class);
    target.date = Utils.findRequiredViewAsType(source, R.id.date, "field 'date'", TextView.class);
    target.time = Utils.findRequiredViewAsType(source, R.id.time, "field 'time'", TextView.class);
    target.videoIcon = Utils.findRequiredViewAsType(source, R.id.video_icon, "field 'videoIcon'", ImageView.class);
    target.profile_image = Utils.findRequiredViewAsType(source, R.id.profile_image, "field 'profile_image'", CircleImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainAppointmentAdapter.MainAppointmentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.docName = null;
    target.date = null;
    target.time = null;
    target.videoIcon = null;
    target.profile_image = null;
  }
}

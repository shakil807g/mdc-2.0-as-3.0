package com.mdconsults.patient.caregiver;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CareGiverFragment_MembersInjector implements MembersInjector<CareGiverFragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public CareGiverFragment_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<CareGiverFragment> create(Provider<ApiFactory> apiFactoryProvider) {
    return new CareGiverFragment_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(CareGiverFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      CareGiverFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

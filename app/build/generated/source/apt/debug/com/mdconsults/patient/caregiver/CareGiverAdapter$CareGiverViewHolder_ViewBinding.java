// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.caregiver;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CareGiverAdapter$CareGiverViewHolder_ViewBinding implements Unbinder {
  private CareGiverAdapter.CareGiverViewHolder target;

  @UiThread
  public CareGiverAdapter$CareGiverViewHolder_ViewBinding(CareGiverAdapter.CareGiverViewHolder target, View source) {
    this.target = target;

    target.mobile = Utils.findRequiredViewAsType(source, R.id.mobile, "field 'mobile'", TextView.class);
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", TextView.class);
    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.realtion = Utils.findRequiredViewAsType(source, R.id.txt_relationship, "field 'realtion'", TextView.class);
    target.txt_share_option = Utils.findRequiredViewAsType(source, R.id.txt_share_option, "field 'txt_share_option'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CareGiverAdapter.CareGiverViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mobile = null;
    target.email = null;
    target.name = null;
    target.realtion = null;
    target.txt_share_option = null;
  }
}

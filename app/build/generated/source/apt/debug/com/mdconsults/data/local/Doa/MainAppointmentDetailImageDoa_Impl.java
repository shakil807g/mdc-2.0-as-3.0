package com.mdconsults.data.local.Doa;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import com.mdconsults.data.remote.model.MainAppointmentDetailImage;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

public class MainAppointmentDetailImageDoa_Impl implements MainAppointmentDetailImageDoa {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfMainAppointmentDetailImage;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public MainAppointmentDetailImageDoa_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMainAppointmentDetailImage = new EntityInsertionAdapter<MainAppointmentDetailImage>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `MainAppointmentDetailImage`(`id`,`recordId`,`image`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MainAppointmentDetailImage value) {
        if (value.id == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.id);
        }
        if (value.recordId == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.recordId);
        }
        if (value.image == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.image);
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM MainAppointmentDetailImage WHERE id = ?";
        return _query;
      }
    };
  }

  @Override
  public void insertAll(List<MainAppointmentDetailImage> mainAppointments) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMainAppointmentDetailImage.insert(mainAppointments);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(String id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (id == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, id);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public List<MainAppointmentDetailImage> getImageDetails(String id) {
    final String _sql = "SELECT * FROM MainAppointmentDetailImage WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfRecordId = _cursor.getColumnIndexOrThrow("recordId");
      final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
      final List<MainAppointmentDetailImage> _result = new ArrayList<MainAppointmentDetailImage>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final MainAppointmentDetailImage _item;
        _item = new MainAppointmentDetailImage();
        _item.id = _cursor.getString(_cursorIndexOfId);
        _item.recordId = _cursor.getString(_cursorIndexOfRecordId);
        _item.image = _cursor.getString(_cursorIndexOfImage);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}

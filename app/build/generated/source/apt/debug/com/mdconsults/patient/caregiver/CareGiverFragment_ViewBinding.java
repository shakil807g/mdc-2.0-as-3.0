// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.caregiver;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CareGiverFragment_ViewBinding implements Unbinder {
  private CareGiverFragment target;

  private View view2131296285;

  @UiThread
  public CareGiverFragment_ViewBinding(final CareGiverFragment target, View source) {
    this.target = target;

    View view;
    target.careGiverList = Utils.findRequiredViewAsType(source, R.id.care_giver_list, "field 'careGiverList'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.add_care_fab, "field 'addCareFab' and method 'careGiverClick'");
    target.addCareFab = Utils.castView(view, R.id.add_care_fab, "field 'addCareFab'", FloatingActionButton.class);
    view2131296285 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.careGiverClick();
      }
    });

    Context context = source.getContext();
    Resources res = context.getResources();
    target.listviewSpace = res.getDimensionPixelSize(R.dimen.item_space_small);
  }

  @Override
  @CallSuper
  public void unbind() {
    CareGiverFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.careGiverList = null;
    target.addCareFab = null;

    view2131296285.setOnClickListener(null);
    view2131296285 = null;
  }
}

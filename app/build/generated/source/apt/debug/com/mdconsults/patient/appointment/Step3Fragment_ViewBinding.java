// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.appointment;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Step3Fragment_ViewBinding implements Unbinder {
  private Step3Fragment target;

  @UiThread
  public Step3Fragment_ViewBinding(Step3Fragment target, View source) {
    this.target = target;

    target.docList = Utils.findRequiredViewAsType(source, R.id.doc_list, "field 'docList'", RecyclerView.class);
    target.searchBySpec = Utils.findRequiredViewAsType(source, R.id.search_by_spec, "field 'searchBySpec'", EditText.class);
    target.search_by_doc_name = Utils.findRequiredViewAsType(source, R.id.search_by_doc_name, "field 'search_by_doc_name'", EditText.class);
    target.search_by_location = Utils.findRequiredViewAsType(source, R.id.search_by_location, "field 'search_by_location'", EditText.class);

    Context context = source.getContext();
    Resources res = context.getResources();
    target.listviewSpace = res.getDimensionPixelSize(R.dimen.item_space_small);
  }

  @Override
  @CallSuper
  public void unbind() {
    Step3Fragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.docList = null;
    target.searchBySpec = null;
    target.search_by_doc_name = null;
    target.search_by_location = null;
  }
}

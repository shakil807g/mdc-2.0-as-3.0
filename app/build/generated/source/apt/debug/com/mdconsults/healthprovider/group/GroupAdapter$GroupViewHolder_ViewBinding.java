// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.group;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GroupAdapter$GroupViewHolder_ViewBinding implements Unbinder {
  private GroupAdapter.GroupViewHolder target;

  @UiThread
  public GroupAdapter$GroupViewHolder_ViewBinding(GroupAdapter.GroupViewHolder target, View source) {
    this.target = target;

    target.txtName = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'txtName'", TextView.class);
    target.txtType = Utils.findRequiredViewAsType(source, R.id.txt_type, "field 'txtType'", TextView.class);
    target.viewColor = Utils.findRequiredViewAsType(source, R.id.view_color, "field 'viewColor'", TextView.class);
    target.btn_delete = Utils.findRequiredViewAsType(source, R.id.btn_delete, "field 'btn_delete'", ImageView.class);
    target.view_group = Utils.findRequiredViewAsType(source, R.id.view_group, "field 'view_group'", ImageView.class);
    target.edit_group = Utils.findRequiredViewAsType(source, R.id.edit_group, "field 'edit_group'", ImageView.class);
    target.add_member = Utils.findRequiredViewAsType(source, R.id.add_member, "field 'add_member'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GroupAdapter.GroupViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtName = null;
    target.txtType = null;
    target.viewColor = null;
    target.btn_delete = null;
    target.view_group = null;
    target.edit_group = null;
    target.add_member = null;
  }
}

package com.mdconsults.di;

import com.mdconsults.data.remote.ApiFactory;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideMDCApiFactory implements Factory<ApiFactory> {
  private final AppModule module;

  private final Provider<Retrofit> retrofitProvider;

  public AppModule_ProvideMDCApiFactory(AppModule module, Provider<Retrofit> retrofitProvider) {
    assert module != null;
    this.module = module;
    assert retrofitProvider != null;
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public ApiFactory get() {
    return Preconditions.checkNotNull(
        module.provideMDCApi(retrofitProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ApiFactory> create(AppModule module, Provider<Retrofit> retrofitProvider) {
    return new AppModule_ProvideMDCApiFactory(module, retrofitProvider);
  }
}

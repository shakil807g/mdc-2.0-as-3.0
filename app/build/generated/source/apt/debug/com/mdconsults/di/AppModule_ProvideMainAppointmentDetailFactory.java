package com.mdconsults.di;

import com.mdconsults.data.local.Doa.MainAppointmentDetailDoa;
import com.mdconsults.data.local.MDCDatabase;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideMainAppointmentDetailFactory
    implements Factory<MainAppointmentDetailDoa> {
  private final AppModule module;

  private final Provider<MDCDatabase> dbProvider;

  public AppModule_ProvideMainAppointmentDetailFactory(
      AppModule module, Provider<MDCDatabase> dbProvider) {
    assert module != null;
    this.module = module;
    assert dbProvider != null;
    this.dbProvider = dbProvider;
  }

  @Override
  public MainAppointmentDetailDoa get() {
    return Preconditions.checkNotNull(
        module.provideMainAppointmentDetail(dbProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MainAppointmentDetailDoa> create(
      AppModule module, Provider<MDCDatabase> dbProvider) {
    return new AppModule_ProvideMainAppointmentDetailFactory(module, dbProvider);
  }

  /** Proxies {@link AppModule#provideMainAppointmentDetail(MDCDatabase)}. */
  public static MainAppointmentDetailDoa proxyProvideMainAppointmentDetail(
      AppModule instance, MDCDatabase db) {
    return instance.provideMainAppointmentDetail(db);
  }
}

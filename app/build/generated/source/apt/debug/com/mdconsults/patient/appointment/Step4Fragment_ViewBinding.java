// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.appointment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Step4Fragment_ViewBinding implements Unbinder {
  private Step4Fragment target;

  private View view2131296665;

  private View view2131296448;

  private View view2131296397;

  private View view2131296475;

  private View view2131296295;

  private View view2131296479;

  private View view2131296450;

  private View view2131296449;

  private View view2131296297;

  private View view2131296627;

  private View view2131296551;

  private View view2131296550;

  private View view2131296352;

  @UiThread
  public Step4Fragment_ViewBinding(final Step4Fragment target, View source) {
    this.target = target;

    View view;
    target.bookSlot = Utils.findRequiredViewAsType(source, R.id.book_slot, "field 'bookSlot'", RelativeLayout.class);
    target.dateSlot = Utils.findRequiredViewAsType(source, R.id.date_slot, "field 'dateSlot'", LinearLayout.class);
    target.timeslot = Utils.findRequiredViewAsType(source, R.id.timeslot, "field 'timeslot'", LinearLayout.class);
    target.no_dates_view = Utils.findRequiredViewAsType(source, R.id.no_dates_view, "field 'no_dates_view'", TextView.class);
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'click'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", Button.class);
    view2131296665 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.click();
      }
    });
    view = Utils.findRequiredView(source, R.id.jan, "method 'onViewClicked'");
    view2131296448 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.feb, "method 'onViewClicked'");
    view2131296397 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mar, "method 'onViewClicked'");
    view2131296475 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.apr, "method 'onViewClicked'");
    view2131296295 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.may, "method 'onViewClicked'");
    view2131296479 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.june, "method 'onViewClicked'");
    view2131296450 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.july, "method 'onViewClicked'");
    view2131296449 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.aug, "method 'onViewClicked'");
    view2131296297 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sep, "method 'onViewClicked'");
    view2131296627 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.oct, "method 'onViewClicked'");
    view2131296551 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.nov, "method 'onViewClicked'");
    view2131296550 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.dec, "method 'onViewClicked'");
    view2131296352 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.months = Utils.listOf(
        Utils.findRequiredViewAsType(source, R.id.jan, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.feb, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.mar, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.apr, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.may, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.june, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.july, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.aug, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.sep, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.oct, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.nov, "field 'months'", CheckedTextView.class), 
        Utils.findRequiredViewAsType(source, R.id.dec, "field 'months'", CheckedTextView.class));
  }

  @Override
  @CallSuper
  public void unbind() {
    Step4Fragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.bookSlot = null;
    target.dateSlot = null;
    target.timeslot = null;
    target.no_dates_view = null;
    target.submit = null;
    target.months = null;

    view2131296665.setOnClickListener(null);
    view2131296665 = null;
    view2131296448.setOnClickListener(null);
    view2131296448 = null;
    view2131296397.setOnClickListener(null);
    view2131296397 = null;
    view2131296475.setOnClickListener(null);
    view2131296475 = null;
    view2131296295.setOnClickListener(null);
    view2131296295 = null;
    view2131296479.setOnClickListener(null);
    view2131296479 = null;
    view2131296450.setOnClickListener(null);
    view2131296450 = null;
    view2131296449.setOnClickListener(null);
    view2131296449 = null;
    view2131296297.setOnClickListener(null);
    view2131296297 = null;
    view2131296627.setOnClickListener(null);
    view2131296627 = null;
    view2131296551.setOnClickListener(null);
    view2131296551 = null;
    view2131296550.setOnClickListener(null);
    view2131296550 = null;
    view2131296352.setOnClickListener(null);
    view2131296352 = null;
  }
}

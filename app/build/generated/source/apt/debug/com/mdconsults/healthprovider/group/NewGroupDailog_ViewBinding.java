// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.group;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import com.mdconsults.widget.MembersView;
import com.mdconsults.widget.SpecialityView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NewGroupDailog_ViewBinding implements Unbinder {
  private NewGroupDailog target;

  private View view2131296325;

  private View view2131296326;

  private View view2131296542;

  private View view2131296568;

  private View view2131296308;

  @UiThread
  public NewGroupDailog_ViewBinding(final NewGroupDailog target, View source) {
    this.target = target;

    View view;
    target.item1 = Utils.findRequiredViewAsType(source, R.id.item_1, "field 'item1'", LinearLayout.class);
    target.item2 = Utils.findRequiredViewAsType(source, R.id.item_2, "field 'item2'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.check_private, "field 'checkPrivate' and method 'onViewClicked'");
    target.checkPrivate = Utils.castView(view, R.id.check_private, "field 'checkPrivate'", CheckedTextView.class);
    view2131296325 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.check_public, "field 'checkPublic' and method 'onViewClicked'");
    target.checkPublic = Utils.castView(view, R.id.check_public, "field 'checkPublic'", CheckedTextView.class);
    view2131296326 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtGroupName = Utils.findRequiredViewAsType(source, R.id.txt_group_name, "field 'txtGroupName'", EditText.class);
    view = Utils.findRequiredView(source, R.id.next_btn, "field 'nextbtn' and method 'nextClicked'");
    target.nextbtn = Utils.castView(view, R.id.next_btn, "field 'nextbtn'", Button.class);
    view2131296542 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.nextClicked();
      }
    });
    target.specialityView = Utils.findRequiredViewAsType(source, R.id.skillview, "field 'specialityView'", SpecialityView.class);
    view = Utils.findRequiredView(source, R.id.pre_btn, "field 'pre_btn' and method 'preClicked'");
    target.pre_btn = Utils.castView(view, R.id.pre_btn, "field 'pre_btn'", Button.class);
    view2131296568 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.preClicked();
      }
    });
    target.viewMember = Utils.findRequiredViewAsType(source, R.id.view_member, "field 'viewMember'", MembersView.class);
    view = Utils.findRequiredView(source, R.id.btn_finish, "field 'btnFinish' and method 'onBtnFinish'");
    target.btnFinish = Utils.castView(view, R.id.btn_finish, "field 'btnFinish'", Button.class);
    view2131296308 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnFinish();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    NewGroupDailog target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.item1 = null;
    target.item2 = null;
    target.checkPrivate = null;
    target.checkPublic = null;
    target.txtGroupName = null;
    target.nextbtn = null;
    target.specialityView = null;
    target.pre_btn = null;
    target.viewMember = null;
    target.btnFinish = null;

    view2131296325.setOnClickListener(null);
    view2131296325 = null;
    view2131296326.setOnClickListener(null);
    view2131296326 = null;
    view2131296542.setOnClickListener(null);
    view2131296542 = null;
    view2131296568.setOnClickListener(null);
    view2131296568 = null;
    view2131296308.setOnClickListener(null);
    view2131296308 = null;
  }
}

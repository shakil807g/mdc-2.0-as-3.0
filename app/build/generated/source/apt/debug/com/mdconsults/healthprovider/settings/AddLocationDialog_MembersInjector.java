package com.mdconsults.healthprovider.settings;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AddLocationDialog_MembersInjector implements MembersInjector<AddLocationDialog> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public AddLocationDialog_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<AddLocationDialog> create(Provider<ApiFactory> apiFactoryProvider) {
    return new AddLocationDialog_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(AddLocationDialog instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      AddLocationDialog instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

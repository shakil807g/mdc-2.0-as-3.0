// Generated code from Butter Knife. Do not modify!
package com.mdconsults.healthprovider.settings;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InviteMemberDailog_ViewBinding implements Unbinder {
  private InviteMemberDailog target;

  private View view2131296440;

  @UiThread
  public InviteMemberDailog_ViewBinding(final InviteMemberDailog target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.invite, "field 'invite' and method 'onViewClicked'");
    target.invite = Utils.castView(view, R.id.invite, "field 'invite'", Button.class);
    view2131296440 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.textView33 = Utils.findRequiredViewAsType(source, R.id.textView33, "field 'textView33'", TextView.class);
    target.editText5 = Utils.findRequiredViewAsType(source, R.id.editText5, "field 'editText5'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    InviteMemberDailog target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.invite = null;
    target.textView33 = null;
    target.editText5 = null;

    view2131296440.setOnClickListener(null);
    view2131296440 = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.mdconsults.patient.caregiver;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddCareGiverDailog_ViewBinding implements Unbinder {
  private AddCareGiverDailog target;

  private View view2131296314;

  private View view2131296286;

  private View view2131296648;

  private View view2131296645;

  @UiThread
  public AddCareGiverDailog_ViewBinding(final AddCareGiverDailog target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.cancel_btn, "field 'cancelBtn' and method 'onViewClicked'");
    target.cancelBtn = Utils.castView(view, R.id.cancel_btn, "field 'cancelBtn'", ImageButton.class);
    view2131296314 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", EditText.class);
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    target.mobile = Utils.findRequiredViewAsType(source, R.id.mobile, "field 'mobile'", EditText.class);
    view = Utils.findRequiredView(source, R.id.add_caregiver, "field 'addCaregiver' and method 'onViewClicked'");
    target.addCaregiver = Utils.castView(view, R.id.add_caregiver, "field 'addCaregiver'", Button.class);
    view2131296286 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.spin_realtion, "method 'onRealationselected'");
    view2131296648 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.onRealationselected(p0, p2);
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    view = Utils.findRequiredView(source, R.id.spin_email, "method 'onShareOption'");
    view2131296645 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.onShareOption(p0, p2);
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddCareGiverDailog target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cancelBtn = null;
    target.name = null;
    target.email = null;
    target.mobile = null;
    target.addCaregiver = null;

    view2131296314.setOnClickListener(null);
    view2131296314 = null;
    view2131296286.setOnClickListener(null);
    view2131296286 = null;
    ((AdapterView<?>) view2131296648).setOnItemSelectedListener(null);
    view2131296648 = null;
    ((AdapterView<?>) view2131296645).setOnItemSelectedListener(null);
    view2131296645 = null;
  }
}

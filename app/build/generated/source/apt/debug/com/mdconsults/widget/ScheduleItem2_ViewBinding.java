// Generated code from Butter Knife. Do not modify!
package com.mdconsults.widget;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mdconsults.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ScheduleItem2_ViewBinding implements Unbinder {
  private ScheduleItem2 target;

  private View view2131296554;

  private View view2131296738;

  private View view2131296414;

  private View view2131296284;

  @UiThread
  public ScheduleItem2_ViewBinding(ScheduleItem2 target) {
    this(target, target);
  }

  @UiThread
  public ScheduleItem2_ViewBinding(final ScheduleItem2 target, View source) {
    this.target = target;

    View view;
    target.toTxt = Utils.findRequiredViewAsType(source, R.id.to_txt, "field 'toTxt'", TextView.class);
    target.fromTxt = Utils.findRequiredViewAsType(source, R.id.from_txt, "field 'fromTxt'", TextView.class);
    target.toTxtAmPm = Utils.findRequiredViewAsType(source, R.id.to_txt_am_pm, "field 'toTxtAmPm'", TextView.class);
    target.fromTxtAmPm = Utils.findRequiredViewAsType(source, R.id.from_txt_am_pm, "field 'fromTxtAmPm'", TextView.class);
    view = Utils.findRequiredView(source, R.id.open_con_check, "field 'openConCheck' and method 'onChecked'");
    target.openConCheck = Utils.castView(view, R.id.open_con_check, "field 'openConCheck'", CheckBox.class);
    view2131296554 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onChecked(p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.to_time, "method 'onViewClicked'");
    view2131296738 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.from_time, "method 'onViewClicked'");
    view2131296414 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add, "method 'onViewClicked'");
    view2131296284 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ScheduleItem2 target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toTxt = null;
    target.fromTxt = null;
    target.toTxtAmPm = null;
    target.fromTxtAmPm = null;
    target.openConCheck = null;

    ((CompoundButton) view2131296554).setOnCheckedChangeListener(null);
    view2131296554 = null;
    view2131296738.setOnClickListener(null);
    view2131296738 = null;
    view2131296414.setOnClickListener(null);
    view2131296414 = null;
    view2131296284.setOnClickListener(null);
    view2131296284 = null;
  }
}

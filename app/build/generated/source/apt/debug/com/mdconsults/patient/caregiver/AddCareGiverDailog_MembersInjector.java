package com.mdconsults.patient.caregiver;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AddCareGiverDailog_MembersInjector
    implements MembersInjector<AddCareGiverDailog> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public AddCareGiverDailog_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<AddCareGiverDailog> create(
      Provider<ApiFactory> apiFactoryProvider) {
    return new AddCareGiverDailog_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(AddCareGiverDailog instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      AddCareGiverDailog instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

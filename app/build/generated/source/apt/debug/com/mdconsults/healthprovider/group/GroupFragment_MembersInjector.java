package com.mdconsults.healthprovider.group;

import com.mdconsults.data.remote.ApiFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GroupFragment_MembersInjector implements MembersInjector<GroupFragment> {
  private final Provider<ApiFactory> apiFactoryProvider;

  public GroupFragment_MembersInjector(Provider<ApiFactory> apiFactoryProvider) {
    assert apiFactoryProvider != null;
    this.apiFactoryProvider = apiFactoryProvider;
  }

  public static MembersInjector<GroupFragment> create(Provider<ApiFactory> apiFactoryProvider) {
    return new GroupFragment_MembersInjector(apiFactoryProvider);
  }

  @Override
  public void injectMembers(GroupFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiFactory = apiFactoryProvider.get();
  }

  public static void injectApiFactory(
      GroupFragment instance, Provider<ApiFactory> apiFactoryProvider) {
    instance.apiFactory = apiFactoryProvider.get();
  }
}

package com.mdconsults.di;

import android.app.Application;
import com.mdconsults.data.local.MDCDatabase;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideDbFactory implements Factory<MDCDatabase> {
  private final AppModule module;

  private final Provider<Application> appProvider;

  public AppModule_ProvideDbFactory(AppModule module, Provider<Application> appProvider) {
    assert module != null;
    this.module = module;
    assert appProvider != null;
    this.appProvider = appProvider;
  }

  @Override
  public MDCDatabase get() {
    return Preconditions.checkNotNull(
        module.provideDb(appProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MDCDatabase> create(AppModule module, Provider<Application> appProvider) {
    return new AppModule_ProvideDbFactory(module, appProvider);
  }

  /** Proxies {@link AppModule#provideDb(Application)}. */
  public static MDCDatabase proxyProvideDb(AppModule instance, Application app) {
    return instance.provideDb(app);
  }
}
